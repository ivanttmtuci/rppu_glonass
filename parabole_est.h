double det_third ( double a11, double a12,double a13,
				  double a21, double a22,double a23,
				  double a31, double a32,double a33 );

double approx_parabola(double *iq,double *x, int x_start, int x_end, double *max_x, double *max_y,double * aa, double *bb, double *cc);

double intersect2(double a1,double b1,double c1,double a2,double b2,double c2,double *xp,double *yp);
double parabole_estimation(double *iq,int max);
double RMS(double *data, int len);

double approx_parabola2(double *iq,int x_start, int x_end);