/*
 * RT1_functions.h
 *
 *  Created on: 24 мая 2017 г.
 *      Author: Ilya
 */
#include "RT_AD.h"
#include "max_approx.h"
#include "parabole_est.h"
//#include "DC_compensation.h"

#define  get(A,B) 				( *(A+B) )

#define TESTS_COUNT 50
#define PSP_COUNT 	1
#define GRAND_TESTS_COUNT 2000000
#define GHz (1000000000)
#define MHz (1000000)


#define SLOT_COUNT 		80

/*
 * Для приемника
 */
#define TD_MODE
#define CALIBRATION
//#define METHOD_NUM 3
/****************************************/


/*
 * Для передатчика
 */
#define EVERY_SLOT // START_SLOT 0 <-- Если требуется передача в каждом слоте
#define START_SLOT		0
/****************************************/


double kk[5];
//u32 etalon[1000][32];

u32 tests_counter;
u32  error_counter;

double calibrated_2_meters[6] ;
//u32 etalon_iter;

double dis_delta, dis_deltamax;
double sum, sum_tr;

void print_double(double dbl, const u32 len);
u32 get_int_part_from_double(double d_num);
u32 get_float_part_from_double(double d_num);
void print_stat(double dis_delta, double dis_deltamax, double sum, double sum_tr);
void print_array(uint32_t* array, uint32_t length);
void print_array_short_int(short int* array, uint32_t length);
void print_array_double(float* array, u32 length);
static inline void usleep(unsigned long usleep);
void mdelay(unsigned long msecs);

u32 read_I_and_Q_corrs();
u32 check_etalon(u32 etalon[32]);
uint32_t max_ind(float *mas, int len);
double max_find(/*int16_t*/ float  *points, int points_len);
u32 start_test_transmission();
u32 stop_transmission();
u32 start_td_psp_transmission_halfsec(float iq[TESTS_COUNT][20], int32_t rtime[2][TESTS_COUNT], double maxes[TESTS_COUNT]);
//u32 start_td_psp_transmission(float iq[TESTS_COUNT][20], int32_t rtime[2][TESTS_COUNT], double maxes[TESTS_COUNT], u32 period, u32 isLoopbackEnabled);
u32 start_td_psp_transmission(u32 period, u32 start_slot, u32 isLoopbackEnabled);
u32 read_corr_buffer_1(float iq[TESTS_COUNT][20], int32_t rtime[2][TESTS_COUNT], double maxes[TESTS_COUNT], int16_t blrci[TESTS_COUNT][20], int16_t blrcq[TESTS_COUNT][20], u32 check_count, u32 len);
u32 read_specific_corr_buffer(u32 target_corr, float iq[TESTS_COUNT][20], int32_t rtime[2][TESTS_COUNT], double maxes[TESTS_COUNT], int16_t blrci[TESTS_COUNT][20], int16_t blrcq[TESTS_COUNT][20], u32 check_count, u32 len);

u32 read_all_corr_buffers(float iq[TESTS_COUNT][20], int32_t rtime[2][TESTS_COUNT], double maxes[TESTS_COUNT], u32 check_count, u32 len);
u32 start_test_psp_transmission();
u32 start_delayed_transmission(u32 period);
u32 psp_scan_init();

u32 start_TR1_loop_with_TD(float iq[TESTS_COUNT][20], int32_t rtime[2][TESTS_COUNT], double maxes[TESTS_COUNT], u32 period);
u32 start_TR1_loop_noTD_fixCount(float iq[TESTS_COUNT][20], int32_t rtime[2][TESTS_COUNT], double maxes[TESTS_COUNT]);
//u32 start_T1_with_TD(float iq[TESTS_COUNT][20], int32_t rtime[2][TESTS_COUNT], double maxes[TESTS_COUNT], u32 period);
u32 start_T1_with_TD(u32 period, u32 start_slot);
u32 calculate_stat_R1(float iq[TESTS_COUNT][20], int32_t rtime[2][TESTS_COUNT], double maxes[TESTS_COUNT]);
void reset_R1_stat(float iq[TESTS_COUNT][20], int32_t rtime[2][TESTS_COUNT], double maxes[TESTS_COUNT]);
void set_initial_timing_conf();
void set_initial_timing_conf_with_ext_sync();
u32 start_test_rcv(uint32_t tresh, uint32_t num);
void start_T1_noTD();
void start_T1_constantly();
u32 get_time_slot_id(double corr_max);

u32 enable_main_transmission_cycle();
u32 enable_main_recv_cycle();

void ch1_fir_tr_load();
void ch1_fir_rec_load();

//extern int16_t iq[TESTS_COUNT][20];
//extern float iq[TESTS_COUNT][20];
//extern int32_t rtime[2][TESTS_COUNT];
//extern double maxes[TESTS_COUNT];
//extern u32 tests_counter;

