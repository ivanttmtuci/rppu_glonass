/*
 * DC_compensation.h
 *
 *  Created on: 18 мая 2018 г.
 *      Author: work
 */

#ifndef SRC_DC_COMPENSATION_H_
#define SRC_DC_COMPENSATION_H_

#include "RT_AD.h"
#include "RPPU_spi_write.h"
#include "RT1_functions.h"

#define IN_S									0x200
#define CNT_mask								0x0000FFE0

#define IFA_mask								0x0000FC00
#define IFA_shift								10

#define COMPENSATION__IDLE_STATE				0x01
#define COMPENSATION__STEP_1					0x02
#define COMPENSATION__STEP_2					0x03
#define COMPENSATION__STEP_3					0x04
#define COMPENSATION__STEP_4					0x05
#define COMPENSATION__END						0x06

#define COMPENSATION__WIDE						0x00
#define COMPENSATION__NARR						0x01

void read_and_print_DC_lvl (void);
void enable_DC_correction_nodriver (uint8_t filter_type);
void read_DC_lvl(void);
void write_CNT_regs(void);
void msu_on(void);
void msu_off(void);
void ifa_write (uint8_t ifa_val);
void IFA_Change (void);

#endif /* SRC_DC_COMPENSATION_H_ */
