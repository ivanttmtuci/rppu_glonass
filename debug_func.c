/*
 * debug_func.c
 *
 *  Created on: 3 ��� 2017 �.
 *      Author: Tatarchuck
 */

#include "xscugic.h"
#include "xil_exception.h"
#include "xdmaps.h"
#include "xdevcfg.h"
#include "xscutimer.h"
#include "xscuwdt.h"

#include "debug_func.h"


unsigned int toggle_bit(unsigned int reg, unsigned int position)
{
	reg=reg^(0x01<<position);
	return reg;

}

void mudelay()
{
	int i=0;
		int a=0;
	while (i<40)
		{
				a=Xil_In32((DATA_3_RO));
				i++;
		}

}

void mudelay_d()
{
	int i=0;
	int a=0;
	while (i<40000)
		{
				a=Xil_In32((DATA_3_RO));
				i++;
		}

}


unsigned int read_one_byte_spi(unsigned int addr)
{

	unsigned int reg_arr[10];
	unsigned int i;
	unsigned int regMSB=0;
	unsigned int regLSB=0;
	unsigned int reg16=0;


	for (i=0; i<10; i++) reg_arr[i]=0;

	// 15 - 0, 14-12 - 0 , 11-10 - x, 9-0 - addr

	reg16= reg16+addr;
	regMSB=reg16>>8;
	regLSB=reg16&0xff;


			Xil_Out32((BA_spi0), (u32)(0x00027C23));  // config spi 0x00027C23 0x00027C3B



			Xil_Out32((BA_spi0), (u32)(0x00027823));  //   nCS0 set 0

			Xil_Out32((BA_spi0 + 0x014), (u32)(0x01)); //   run spi
			//  SPI ��������� � MSB ������ �� ���������



			Xil_Out32((Tx_data_reg0), (u32)(regMSB)); //
			Xil_Out32((Tx_data_reg0), (u32)(regLSB)); //

			Xil_Out32((Tx_data_reg0), (u32)(0x00)); // ���� ���� �������� ���� �������� ������


			//Xil_Out32((BA_spi0 + 0x08), (u32)(0x27));  // enable interrupts ...
			mudelay();
			//mudelay();
	    	for (i=0; i<3; i++) reg_arr[i]=Xil_In32((Rx_data_reg0));
	    	// For every �n� bytes written to the TxFIFO, there will be �n�
	    			//bytes stored in RxFIFO that must be read by software before starting the next transfer.
			//Xil_Out32((BA_spi0 + 0x0C), (u32)(0x27));  // disable interrupts ...
			Xil_Out32((BA_spi0 + 0x14), (u32)(0x00)); //   stop spi
			Xil_Out32((BA_spi0), (u32)(0x00027C23));  //   nCS0 set 0
			mudelay();
			return reg_arr[2];
}
unsigned int write_one_byte_spi(unsigned int addr, unsigned int wr_reg)
{

		unsigned int regMSB=0;
		unsigned int regLSB=0;
		unsigned int reg16=0;
		unsigned int i=0;
		unsigned int reg_arr[10];
		// 15 - 1, 14-12 - 0 , 11-10 - x, 9-0 - addr

		reg16=toggle_bit(reg16,15); // ������� ��� - ������ ������

		reg16= reg16+addr;
		regMSB=reg16>>8;
		regLSB=reg16&0xff;

		Xil_Out32((BA_spi0), (u32)(0x00027C23));  // config spi 0x00027C23

		Xil_Out32((BA_spi0), (u32)(0x00027823));  //   nCS0 set 0

		Xil_Out32((BA_spi0 + 0x014), (u32)(0x01)); //   run spi
				//  SPI ��������� � MSB ������ �� ���������

		Xil_Out32((Tx_data_reg0), (u32)(regMSB)); //
		Xil_Out32((Tx_data_reg0), (u32)(regLSB)); //
		Xil_Out32((Tx_data_reg0), (u32)(wr_reg)); // ���� ���� �������� ���� �������� ������

		//Xil_Out32((Tx_data_reg0), (u32)(0x00));





		mudelay_d();
		mudelay_d();
		mudelay_d();
		mudelay_d();
		mudelay_d();
		mudelay_d();
		for (i=0; i<4; i++) reg_arr[i]=Xil_In32((Rx_data_reg0));
		// For every �n� bytes written to the TxFIFO, there will be �n�
				//bytes stored in RxFIFO that must be read by software before starting the next transfer.
		Xil_Out32((BA_spi0 + 0x14), (u32)(0x00)); //   stop spi
		Xil_Out32((BA_spi0), (u32)(0x00027C23));  //   nCS0 set 0

		mudelay();
	return 1;
}
