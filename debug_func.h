#define BA_gpio  0xE000A000
#define BA_spi0  0xE0006000


#define MASK_DATA_3_LSW (BA_gpio+0x018)
#define DIRM_3          (BA_gpio+0x2C4)   // 1 for output , 0 for input
#define OEN_3           (BA_gpio+0x2C8)	  // 1 for output enabled , 0 - disabledb
#define DATA_3          (BA_gpio+0x04c)
#define DATA_3_RO       (BA_gpio+0x06c)

#define  Tx_data_reg0  (0xE0006000+0x0000001C)
#define  Rx_data_reg0  (0xE0006000+0x00000020)


void mudelay();
void mudelay_d();
unsigned int read_one_byte_spi(unsigned int addr);
unsigned int toggle_bit(unsigned int reg, unsigned int position);
unsigned int write_one_byte_spi(unsigned int addr, unsigned int wr_reg);
