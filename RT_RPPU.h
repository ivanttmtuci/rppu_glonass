/*
 * RT_RPPU.h
 *
 *  Created on: 19 марта 2018 г.
 *      Author: Ivan
 */

#include "RPPU_API.H"


#include "RPPU_spi_write.h"

#ifndef SRC_RT_RPPU_H_
#define SRC_RT_RPPU_H_


/***************************************************/


/*   настройка РППУ в режим приемопередатчика 1 канала   */
uint32_t  rppu_1ch_tr_rec_setup(  struct  rppu_rf *phy, uint8_t amppify);





/***************************************************/
#endif /* SRC_RT_RPPU_H_ */



