/*
 * RT1_functions.c
 *
 *  Created on: 24 мая 2017 г.
 *      Author: Ilya
 */

#include "RT1_functions.h"
#include "RT_AD.h"
#include "RT1_functions.h"
#include "RPPU.h"
#include "RT_RPPU.h"
#include "DC_compensation.h"
#include "lagrange.h"
#include "sync.h"

void print_double(double dbl, const u32 len)
{
	char dbl_ch[len];
	const char ch_len = len + '0';
	const char conf[5] = {'%','2','.',ch_len,'f'};
	sprintf(dbl_ch, conf, dbl);
//	mdelay(250);
	xil_printf("%s", dbl_ch);

}

void print_array(uint32_t* array, uint32_t length)
{
	for (int i = 0; i < length; i++)
	{
		printf("%u", array[i]);
		printf(" ");
	}
	print("\r\n");
}

void print_array_double(float array[], u32 length)
{
	for (u32 i = 0; i < length; i++)
	{
		printf("%u : %f\r\n", i, array[i]);
	}
	print("\r\n");
}

void print_array_short_int(short int* array, uint32_t length)
{
	for (int i = 0; i < length; i++)
	{
		printf("%d", array[i]);
		printf(" ");
	}
	printf("\r\n");
}

uint32_t max_ind(/*int16_t*/ float *mas, int len)
{
	int res = -32767;
	int ind = 0, i;

	int fix=0;
	for(i = 0; i < len; i++)
	{
		fix=(int)(mas[i]*32767.0);
		if(res < mas[i])
		{
			res = mas[i];
			ind = i;
		}
	}
	return ind;
}


double max_find(/*int16_t */ float *points, int points_len)
{

	// будем полагать, что нам пока не нужно выбирать точки тщательно. Простого разбиения на 2 нам хватит.


	float z, o, p, q;

	float k1, k2;
	float b1, b2;

	float maxy, maxx;

	float det, detx, dety;

	int start_pos_right;
	int i;

	int x_start = 0;



	x_start = max_ind(points, 20);
	start_pos_right = x_start + 1;  //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

	//*********************************************ПОИСК КОЭФФИЦИЕНТОВ ПРЯМЫХ*******************************


	//********************************ЛЕВАЯ ПРЯМАЯ*******************************************************

	// ===========определеник коэфф в системе нормальных уравнений НАЧАЛО==============

	// summ(x*x) - z

	//z=  float   ( get(points,0)*get(points,0)   +get(points,1)*get(points,1)+get(points,2)*get(points,2)+get(points,3)*get(points,3)  +get(points,4)*get(points,4)    )
	z = 0;
	q = 0;
	o = 0;
	p = 0;



	for (i = x_start-1; i>x_start - points_len; i--)
	{
		z = z + (float)(i*i);
	}

	// summ y - q

	//q= (float)  (  get(points,0) + get(points,1)+ get(points,2)+ get(points,3)  + get(points,4) );

	for (i = x_start - 1; i>x_start - points_len; i--)
	{
		q = q + (float)get(points, i);
	}


	// summ x - p

	//p=(float)  (0+1+2+3+4);

	for (i = x_start - 1; i>x_start-points_len; p += i, i--);



	// o - x*y

	//o=(float )  (    get(points,0)*0+ get(points,1)*1+ get(points,2)*2+ get(points,3)*3+ get(points,4)*4       );
	for (i = x_start - 1; i>x_start-points_len; i--)
	{
		o = o + (float)(get(points, i)*i);
	}



	// ===========определеник коэфф в системе нормальных уравнений  КОНЕЦ==============

	// ищем определители

	det = z*points_len - p*p;

	detx = o*points_len - p*q;

	dety = z*q - o*p;

	k1 = detx / det;
	b1 = dety / det;


	//************************ПРАВАЯ ПРЯМАЯ*************************************************



	// ===========определеник коэфф в системе нормальных уравнений НАЧАЛО==============

	// summ(x*x) - z

	//z=  float   ( get(points,0)*get(points,0)   +get(points,1)*get(points,1)+get(points,2)*get(points,2)+get(points,3)*get(points,3)  +get(points,4)*get(points,4)    )
	z = 0;
	q = 0;
	o = 0;
	p = 0;
	for (i = start_pos_right; i<start_pos_right + points_len; i++)
	{
		z = z + (float)(i*i);
	}

	// summ y - q

	//q= (float)  (  get(points,0) + get(points,1)+ get(points,2)+ get(points,3)  + get(points,4) );

	for (i = start_pos_right; i<start_pos_right + points_len; i++)
	{
		q = q + (float)get(points, i);
	}


	// summ x - p

	//p=(float)  (0+1+2+3+4);

	for (i = start_pos_right; i<(start_pos_right + points_len); p += i, i++);



	// o - x*y

	//o=(float )  (    get(points,0)*0+ get(points,1)*1+ get(points,2)*2+ get(points,3)*3+ get(points,4)*4       );
	for (i = start_pos_right; i<start_pos_right + points_len; i++)
	{
		o = o + (float)(get(points, i)*i);
	}



	// ===========определеник коэфф в системе нормальных уравнений  КОНЕЦ==============


	// ищем определители

	det = z*points_len - p*p;

	detx = o*points_len - p*q;

	dety = z*q - o*p;

	k2 = detx / det;
	b2 = dety / det;


	//***********************************КОЭФФИЦИЕНТЫ НАЙДЕНЫ**************************************



	maxx = (b2 - b1) / (k1 - k2);
	maxy = k1*maxx + b1;

	return maxx;
}

u32 start_test_rcv(uint32_t tresh, uint32_t num)
{
	psp_scan_init();

	u32 b;

	Xil_Out32((BA_R1 + 0x14), (u32)(tresh));			//Установка уровня порога
	Xil_Out32((BA_R1 + 0x18), (u32)(num));			//Номер регистра для сравнения с порогом

	Xil_Out32((BA_R1 + 0x1C), (u32)(8));

	Xil_Out32((BA_R1 + 0x0), (u32)(536870912));	//1.25 МГц

	Xil_Out32((BA_RC), (u32)(0x01));				//Включение тактовой приемника

	return 1;
}

u32 start_test_transmission()
{
	/*
	 * Блок заполнения памяти сканера ПСП
	 */
	for(u32 i = 0; i < 32768; i ++)
			Xil_Out32((BA_mem + i*4),(u32)0);

	//-------------SCANER_MEMORY_INIT---------------------
	Xil_Out32((BA_GPSP),        (u32)(0));
	Xil_Out32((BA_GPSP + 0x04), (u32)(0));
	Xil_Out32((BA_GPSP + 0x08), (u32)(0));
	Xil_Out32((BA_GPSP + 0x0C), (u32)(0));
	Xil_Out32((BA_GPSP + 0x10), (u32)(0));
	Xil_Out32((BA_GPSP + 0x14), (u32)(0));
	Xil_Out32((BA_GPSP + 0x18), (u32)(0));
	Xil_Out32((BA_GPSP + 0x1C), (u32)(0));
	Xil_Out32((BA_GPSP + 0x20), (u32)(0));
	Xil_Out32((BA_GPSP + 0x24), (u32)(0x204));
	Xil_Out32((BA_GPSP + 0x28), (u32)(0x3FF));
	Xil_Out32((BA_GPSP + 0x2C), (u32)(0));
	Xil_Out32((BA_GPSP + 0x30), (u32)(0));
	Xil_Out32((BA_GPSP + 0x34), (u32)(0));
	Xil_Out32((BA_GPSP + 0x38), (u32)(0));
	Xil_Out32((BA_GPSP + 0x3C), (u32)(0));
	Xil_Out32((BA_GPSP + 0x40), (u32)(0));
	Xil_Out32((BA_GPSP + 0x44), (u32)(0));
	Xil_Out32((BA_GPSP + 0x48), (u32)(0));
	Xil_Out32((BA_GPSP + 0x4C), (u32)(0));
	Xil_Out32((BA_GPSP + 0x50), (u32)(0));
	Xil_Out32((BA_GPSP + 0x54), (u32)(0));
	Xil_Out32((BA_GPSP + 0x58), (u32)(0));
	Xil_Out32((BA_GPSP + 0x5C), (u32)(0));
	Xil_Out32((BA_GPSP + 0x60), (u32)(0));
	Xil_Out32((BA_GPSP + 0x64), (u32)(0));
	Xil_Out32((BA_GPSP + 0x68), (u32)(0));
	Xil_Out32((BA_GPSP + 0x6C), (u32)(0));
	Xil_Out32((BA_GPSP + 0x70), (u32)(0));
	Xil_Out32((BA_GPSP + 0x74), (u32)(0));
	Xil_Out32((BA_GPSP + 0x78), (u32)(0));
	Xil_Out32((BA_GPSP + 0x7C), (u32)(0));
	Xil_Out32((BA_GPSP + 0x80), (u32)(0));
	Xil_Out32((BA_GPSP + 0x84), (u32)(0));
	Xil_Out32((BA_GPSP + 0x88), (u32)(0));
	Xil_Out32((BA_GPSP + 0x8C), (u32)(0));
	Xil_Out32((BA_GPSP + 0x90), (u32)(0));

	/*
	Xil_Out32((BA_mem),        (u32)(0xacd6ea65));
	Xil_Out32((BA_mem + 0x04), (u32)(0xb2bec7e0));
	Xil_Out32((BA_mem + 0x08), (u32)(0xbc4ef0f5));
	Xil_Out32((BA_mem + 0x0C), (u32)(0x69a69432));
	Xil_Out32((BA_mem + 0x10), (u32)(0x8c809f98));
	Xil_Out32((BA_mem + 0x14), (u32)(0xcc249e6d));
	Xil_Out32((BA_mem + 0x18), (u32)(0x7b27127f));
	Xil_Out32((BA_mem + 0x1C), (u32)(0x8943fb26));
	Xil_Out32((BA_mem + 0x20), (u32)(0xc20e01f9));
	Xil_Out32((BA_mem + 0x24), (u32)(0xc8e136cf));
	Xil_Out32((BA_mem + 0x28), (u32)(0xbdbb3ced));
	Xil_Out32((BA_mem + 0x2C), (u32)(0x0b0753b3));
	Xil_Out32((BA_mem + 0x30), (u32)(0x697b98d0));
	Xil_Out32((BA_mem + 0x34), (u32)(0xd48194ec));
	Xil_Out32((BA_mem + 0x38), (u32)(0x16f99937));
	Xil_Out32((BA_mem + 0x3C), (u32)(0x8e4ff10b));
	Xil_Out32((BA_mem + 0x40), (u32)(0x851806dc));
	Xil_Out32((BA_mem + 0x44), (u32)(0x45c43371));
	Xil_Out32((BA_mem + 0x48), (u32)(0x74636eec));
	Xil_Out32((BA_mem + 0x4C), (u32)(0x3b59b6a5));
	Xil_Out32((BA_mem + 0x50), (u32)(0x2cd489c5));
	Xil_Out32((BA_mem + 0x54), (u32)(0xe49d5233));
	Xil_Out32((BA_mem + 0x58), (u32)(0xf90cc669));
	Xil_Out32((BA_mem + 0x5C), (u32)(0xeb295c50));
	Xil_Out32((BA_mem + 0x60), (u32)(0x5da7e865));
	Xil_Out32((BA_mem + 0x64), (u32)(0x88c825ac));
	Xil_Out32((BA_mem + 0x68), (u32)(0x801e79cb));
	Xil_Out32((BA_mem + 0x6C), (u32)(0xcf03e65c));
	Xil_Out32((BA_mem + 0x70), (u32)(0x93d42610));
	Xil_Out32((BA_mem + 0x74), (u32)(0xcc667e4c));
	Xil_Out32((BA_mem + 0x78), (u32)(0x2c10ce82));
	Xil_Out32((BA_mem + 0x7C), (u32)(0x37cc30ca));
	Xil_Out32((BA_mem + 0x80), (u32)(0xcc667e4c));
	Xil_Out32((BA_mem + 0x84), (u32)(0x5da7e865));
	Xil_Out32((BA_mem + 0x88), (u32)(0xcc667e4c));
	Xil_Out32((BA_mem + 0x8C), (u32)(0xeb295c50));
	Xil_Out32((BA_mem + 0x90), (u32)(0xcc667e4c));
	*/

	//Xil_Out32((BA_GPSP + 0x24), (u32)(0x204));		//Полином Т - Выходная ПСП
	//Xil_Out32((BA_GPSP + 0x28), (u32)(0x3FF));		//Начальное состояние полинома Т



	//Xil_Out32((BA_GPSP + 0x5C), (u32)(0x01));		//Коэф. Y:kyy
	//Xil_Out32((BA_GPSP + 0x68), (u32)(0x01));		//Приращ. Y:kyn
	Xil_Out32((BA_GPSP),        (u32)(0x01));		//Регистр управления control_reg

	Xil_Out32((BA_RC + 0x14), (u32)(0x01));			//Включ. такт. частоты генератора ПСП

	Xil_Out32((BA_T1),        (u32)(0x00000002));	//Посылается 0 бит данных (2002 для посылки другого к-ва)
	Xil_Out32((BA_T1 + 0x04), (u32)(0x000000D9));	//Регистр данных для передачи 1
	Xil_Out32((BA_T1 + 0x04), (u32)(0xFFFFFFFF));	//Регистр данных для передачи 2
	Xil_Out32((BA_T1 + 0x10), (u32)(429497));		//Код шага частоты модуляции

	u32 b;
	for (u32 i=0; i<10000; i++) b=Xil_In32(BA_T1);	//Задержка

	//Настройка приёмника
	Xil_Out32((BA_R1 + 0x14), (u32)(5000));			//Установка уровня порога
	Xil_Out32((BA_R1 + 0x18), (u32)(4));			//Номер регистра для сравнения с порогом

	Xil_Out32((BA_RC), (u32)(0x03));				//Обнуление приемника и передатчика 1 канала

	Xil_Out32((BA_T1 + 0x0C), (u32)(0x00000001));	//Включение передатчика

	return 1;
}

u32 stop_transmission()
{
	Xil_Out32((BA_GPSP),      (u32)(0x00));			//Выключение сканера ПСП
	Xil_Out32((BA_RC + 0x14), (u32)(0x00));			//Обнуление генератора ПСП
	Xil_Out32((BA_T1 + 0x0C), (u32)(0x00000000));	//Выключение передатчика
	Xil_Out32((BA_RC),        (u32)(0x00));			//Обнуление приемника и передатчика 1 канала

	mdelay(200);
	return 1;
}

/*
u32 start_td_psp_transmission_halfsec(float iq[TESTS_COUNT][20], int32_t rtime[2][TESTS_COUNT], double maxes[TESTS_COUNT])
{
	u32 interval = 0, b = 0 ,f = 0;

	//-------------SCANER_MEMORY_INIT---------------------
	Xil_Out32((BA_mem),        (u32)(0));
	Xil_Out32((BA_mem + 0x04), (u32)(0));
	Xil_Out32((BA_mem + 0x08), (u32)(0));
	Xil_Out32((BA_mem + 0x0C), (u32)(0));
	Xil_Out32((BA_mem + 0x10), (u32)(0));
	Xil_Out32((BA_mem + 0x14), (u32)(0));
	Xil_Out32((BA_mem + 0x18), (u32)(0));
	Xil_Out32((BA_mem + 0x1C), (u32)(0));
	Xil_Out32((BA_mem + 0x20), (u32)(0));
	Xil_Out32((BA_mem + 0x24), (u32)(0x204));
	Xil_Out32((BA_mem + 0x28), (u32)(0x355)); //////////
	Xil_Out32((BA_mem + 0x2C), (u32)(0));
	Xil_Out32((BA_mem + 0x30), (u32)(0));
	Xil_Out32((BA_mem + 0x34), (u32)(0));
	Xil_Out32((BA_mem + 0x38), (u32)(0));
	Xil_Out32((BA_mem + 0x3C), (u32)(0));
	Xil_Out32((BA_mem + 0x40), (u32)(0));
	Xil_Out32((BA_mem + 0x44), (u32)(0));
	Xil_Out32((BA_mem + 0x48), (u32)(0));
	Xil_Out32((BA_mem + 0x4C), (u32)(0));
	Xil_Out32((BA_mem + 0x50), (u32)(0));
	Xil_Out32((BA_mem + 0x54), (u32)(0));
	Xil_Out32((BA_mem + 0x58), (u32)(0));
	Xil_Out32((BA_mem + 0x5C), (u32)(0));
	Xil_Out32((BA_mem + 0x60), (u32)(0));
	Xil_Out32((BA_mem + 0x64), (u32)(0));
	Xil_Out32((BA_mem + 0x68), (u32)(0));
	Xil_Out32((BA_mem + 0x6C), (u32)(0));
	Xil_Out32((BA_mem + 0x70), (u32)(0));
	Xil_Out32((BA_mem + 0x74), (u32)(0));
	Xil_Out32((BA_mem + 0x78), (u32)(0));
	Xil_Out32((BA_mem + 0x7C), (u32)(0));
	Xil_Out32((BA_mem + 0x80), (u32)(0));
	Xil_Out32((BA_mem + 0x84), (u32)(0));
	Xil_Out32((BA_mem + 0x88), (u32)(0));
	Xil_Out32((BA_mem + 0x8C), (u32)(0));
	Xil_Out32((BA_mem + 0x90), (u32)(0));

	Xil_Out32((BA_T1 + 0x0C), (u32)(0x0));
	Xil_Out32((BA_GPSP), (u32)(0x0));

	set_initial_timing_conf();

	Xil_Out32((BA_R1 + 0x14), (u32)(0x7FF));			//Установка уровня порога
	Xil_Out32((BA_R1 + 0x18), (u32)(0x2));			//Номер регистра для сравнения с порогом

	interval = Xil_In32(BA_RTC + 0x08); // прочитать текущее значение долей секунд

	секундный интервал делится на 4 части.
	в каждом поддиапазоне происходит включение/выключение передатчика
	переменная b определяет, какому поддиапазону принадлежит текущее время


	if((interval >= 1000000) && (interval < 5000000))
		b = 0;
	else if((interval >= 5000000) && (interval < 11000000))
		b = 1;
	else if((interval >= 11000000) && (interval < 15000000))
		b = 2;
	else
		b = 3;

	Xil_Out32((BA_GPSP), (u32)(0x0));


	while (tests_counter != TESTS_COUNT)
	{
		//переменная f определяет, какому поддиапазону принадлежит текущее время
		interval = Xil_In32(BA_RTC + 0x08);
//		printf("%u\n", interval);
		if((interval >= 1000000) && (interval < 5000000))
					f = 0;
				else if((interval >= 5000000) && (interval < 11000000))
					f = 1;
				else if((interval >= 11000000) && (interval < 15000000))
					f = 2;
				else
					f = 3;

		// проверка, был ли переход из одного поддиапазона в другой
		// и включение/выключение передатчика соответственно
		if(((b == 3) && (f == 0)) || ((b == 1) && (f == 2)))
		{
			//off
			//u32 temp = Xil_In32(BA_RC + 0x14);
			Xil_Out32((BA_GPSP), (u32)(0x0));				//Выключение сканера ПСП
			Xil_Out32((BA_RC + 0x14), (u32)(0x00));			//Обнуление генератора ПСП
			Xil_Out32((BA_T1 + 0x0C), (u32)(0x00000000));	//Выключение передатчика
			Xil_Out32((BA_RC), (u32)(0x03));				//Обнуление приемника и передатчика 1 канала
		}
		if((b == 0) && (f == 1))
		{
			//on
			// включение передатчика и ГПСП

			Xil_Out32((BA_T1 + 0x0C), (u32)(0x00));

			Xil_Out32((BA_GPSP + 0x24), (u32)(0x204));		//Полином Т - Выходная ПСП
			Xil_Out32((BA_GPSP + 0x28), (u32)(0x3FF));		//Начальное состояние полинома Т
			Xil_Out32((BA_GPSP + 0x9C), (u32)(7000000));	//Время старта ген. ПСП в пред. секунды
			Xil_Out32((BA_RC + 0x14), (u32)(0x1));			//Включ. такт. частоты генератора ПСП
			Xil_Out32((BA_GPSP), (u32)(0x81)); 				// mode 1 SINH

			Xil_Out32((BA_T1), (u32)(0x2));					//Кодирование 4-мя фазами
			Xil_Out32((BA_T1 + 0x10), (u32)(0));			//Код шага частоты модуляции


			Xil_Out32((BA_T1 + 0x14), (u32)(8000000)); 		//Задержка 0.4 сек
			Xil_Out32((BA_T1 + 0x04), (u32)(0x0));			//Рег. данных 1
			Xil_Out32((BA_T1 + 0x08), (u32)(0x0));			//Рег. данных 2


			Xil_Out32((BA_RC), (u32)(0x03));				//Включение тактовой передатчика
			Xil_Out32((BA_T1 + 0x0C), (u32)(0x00000002));	//Включение передатчика по таймеру

			read_corr_buffer_1(iq, rtime, maxes, PSP_COUNT, (u32)3);
			tests_counter += 1;
		}
		if((b == 2) && (f == 3))
		{
			//on

			Xil_Out32((BA_T1 + 0x0C), (u32)(0x00));

			Xil_Out32((BA_GPSP + 0x24), (u32)(0x204));		//Полином Т - Выходная ПСП
			Xil_Out32((BA_GPSP + 0x28), (u32)(0x3FF));      //Начальное состояние полинома Т
			Xil_Out32((BA_GPSP + 0x9C), (u32)(17000000));   //Время старта ген. ПСП в пред. секунды
			Xil_Out32((BA_RC + 0x14), (u32)(0x1));          //Включ. такт. частоты генератора ПСП
			Xil_Out32((BA_GPSP), (u32)(0x81));              // mode 1 SINH


			Xil_Out32((BA_T1), (u32)(0x2));					//Кодирование 4-мя фазами
			Xil_Out32((BA_T1 + 0x10), (u32)(0));            //Код шага частоты модуляции


			Xil_Out32((BA_T1 + 0x14), (u32)(18000000)); 	//Задержка 0.9 сек
			Xil_Out32((BA_T1 + 0x04), (u32)(0x0));          //Рег. данных 1
			Xil_Out32((BA_T1 + 0x08), (u32)(0x0));          //Рег. данных 2


			Xil_Out32((BA_RC), (u32)(0x03));                //Включение тактовой передатчика
			Xil_Out32((BA_T1 + 0x0C), (u32)(0x00000002));   //Включение передатчика по таймеру

			read_corr_buffer_1(iq, rtime, maxes, PSP_COUNT, (u32)3);
			tests_counter += 1;
		}
		b = f; //текущее значение поддиапазона становится предыдущим

	}
	return 1;
}
*/


/*
 * Отправка ПСП с периодом period
 * Значение period - к-во тактов (отсчетов внутри секунды), должно быть целым делителем 20000000
 * Передача начинается с начала следующей секунды
 */

u32 start_td_psp_transmission(u32 period, u32 start_slot, u32 isLoopbackEnabled)
{
	u32 part_second = 0, sec_count = 0;
	u32 slot_count_in_sec = 20000000 / period, slot_num = 0;
//	int32_t prev_slot_num = -1;
	u32 offset = 25000; // Время на передачу после старта передатчика, теор. 1 мсек = 20480 отсчетов

	while (1)
	{
		if (start_slot == 0)
		{
//			xil_printf("Slot Number %u\n", slot_num);
			start_delayed_transmission(period * slot_num); // Для слота, начинающегося с нуля следующей секунды
			tests_counter++;
			/*
			 * Для loopback
			 * Ошибка может возникнуть, если время работы read_corr_buffers() после поднятия флага
			 * будет больше, чем period
			 */

//			if (isLoopbackEnabled == 1)
//			{
//				read_corr_buffer_1(PSP_COUNT, (uint32_t) 3);
//				tests_counter += PSP_COUNT;
//				if (tests_counter >= TESTS_COUNT)
//					return 1;
//			}
		}

		// Дожидаемся пока наступит первый слот (с нуля следующей секунды)
		do {
			part_second = Xil_In32((u32) (BA_RTC + 0x08));
		} while (part_second >= period);

		while (slot_num < slot_count_in_sec - 1)
		{
			do {
				part_second = Xil_In32((u32) (BA_RTC + 0x08));
			} while (part_second <= period * slot_num + offset);

			slot_num++;
#ifndef EVERY_SLOT
			if (slot_num == start_slot || slot_num == start_slot + 4 || slot_num == start_slot + 8 ||
					slot_num == start_slot + 12 || slot_num == start_slot + 16 || slot_num == start_slot + 20 ||
					slot_num == start_slot + 24 || slot_num == start_slot + 28 || slot_num == start_slot + 32 ||
					slot_num == start_slot + 36)
			{
#endif
//				xil_printf("Slot Number %u\n", slot_num);
				start_delayed_transmission(period * slot_num);
				tests_counter++;

				// Для loopback
//				if (isLoopbackEnabled == 1)
//				{
//					read_corr_buffer_1(PSP_COUNT, (uint32_t) 3);
//					tests_counter += PSP_COUNT;
//					if (tests_counter >= TESTS_COUNT)
//						return 1;
//				}
#ifndef EVERY_SLOT
			}
#endif
		}

		do {
			part_second = Xil_In32((u32) (BA_RTC + 0x08));
		} while (part_second <= period * slot_num + offset); // Смещение от начала слота - 1000000 (половина слота)

		slot_num = 0;
	}
	return 1;
}

/*
 * Проверка на превышение порога и считывание значений регистров
 * uint32_t check_count - количество принимаемых сигналов (пиков)
 * target_corr - номер коррелятора от 0 до 10 (вроде)
 */
u32 read_specific_corr_buffer(u32 target_corr, float iq[TESTS_COUNT][20], int32_t rtime[2][TESTS_COUNT], double maxes[TESTS_COUNT], int16_t blrci[TESTS_COUNT][20], int16_t blrcq[TESTS_COUNT][20], u32 check_count, u32 len)
{
//	int16_t blrci[5][20] = {0}, blrcq[5][20] = {0};
	u32 a, c, posb, ni;
	a = c = posb = ni = 0;

	while (c < check_count)
	{
		a=Xil_In32((BA_R1  + 0x40));        //Чтение регистра флагов превышения порога
		if (a != 0)
		{
			if ((a & 0x1 << target_corr) == 0x1 << target_corr)
			{
				read_I_and_Q_corrs();
				u32 i = target_corr;          //Порядковый номер бита в регистре флагов превышения
				for (u32 j=0; j<20; j++)  //Считывание значений буферных регистров + рег. врем. прев. порога (20)
				{
					blrci[tests_counter + c][j] = Xil_In16((BA_R1 + 0x80 * (i + 1) + 4 * j));    //Тут есть приведение в Xil_In16
					blrcq[tests_counter + c][j] = Xil_In16((BA_R1 + 0x80 * (i + 1) + 4 * j + 2));
				}
				rtime[0][tests_counter + c] = Xil_In32((BA_R1 + 0x80 * (i + 1) + 4 * 20));      //Время переполнения порога
				rtime[1][tests_counter + c] = Xil_In32((u32) (BA_RTC + 0x10));            //Значение счетчика секунд
	//          printf("Rcvd: %u sec, %u \n", rtime[1][tests_counter + c], rtime[0][tests_counter + c]);
	//          printf("%u tests_counter\n", tests_counter + c);
				c++;
			}
		}
	}

//	for(u32 i = 0; i < check_count; i++)
//		for(u32 j = 0; j < 20; j++)
//		{
//			if(abs(blrci[i][j]) > abs(blrcq[i][j]))
//				iq[tests_counter + i][j] = abs(blrci[i][j]) + abs(blrcq[i][j])/2;
//			else
//				iq[tests_counter + i][j] = abs(blrcq[i][j]) + abs(blrci[i][j])/2;
//		}

	for(u32 i = 0; i < check_count; i++)
		for(u32 j = 0; j < 20; j++)
		{
			iq[tests_counter + i][j]=sqrt(pow(abs(blrci[tests_counter + i][j]),2)+pow(abs(blrcq[tests_counter + i][j]),2));
		}

	for (int i = 0; i < check_count; i++)
	{
		double y[20] = {0};
		for (uint32_t j = 0; j < 20; j++)
			y[j] = iq[tests_counter + i][j];

//		maxes[tests_counter + i] =  parabole_estimation(y,max_ind(iq[tests_counter + i], 20));		// От Ивана
//		maxes[tests_counter + i] = approx_parabola2(y, 0, 19);


//		maxes[tests_counter + i] = max_find(iq[tests_counter + i], len);
//		maxes[tests_counter + i] =  max_find_smith(1e-5,blrci[tests_counter + i],blrcq[tests_counter + i]);
//		maxes[tests_counter + i]= (double) max_ind(iq[tests_counter + i], 20);
//		maxes[tests_counter + i] += (double)rtime[0][tests_counter + i] - 6.0;
	}
	return 0;
}

/*
 * Проверка на превышение порога и считывание значений регистров
 * uint32_t check_count - количество принимаемых сигналов (пиков)
 */
u32 read_corr_buffer_1(float iq[TESTS_COUNT][20], int32_t rtime[2][TESTS_COUNT], double maxes[TESTS_COUNT], int16_t blrci[TESTS_COUNT][20], int16_t blrcq[TESTS_COUNT][20], u32 check_count, u32 len)
{
	u32 target_corr = 1;
//	int16_t blrci[5][20] = {0}, blrcq[5][20] = {0};
	u32 a, c, posb, ni;
	a = c = posb = ni = 0;

	while (c < check_count)
	{
		a=Xil_In32((BA_R1  + 0x40));        //Чтение регистра флагов превышения порога
		if ((a & 0x1 ) == target_corr)
		{
			read_I_and_Q_corrs();
			u32 i = 0;          //Порядковый номер бита в регистре флагов превышения
			for (u32 j=0; j<20; j++)  //Считывание значений буферных регистров + рег. врем. прев. порога (20)
			{
				blrci[tests_counter + c][j] = Xil_In16((BA_R1 + 0x80 * (i + 1) + 4 * j));    //Тут есть приведение в Xil_In16
				blrcq[tests_counter + c][j] = Xil_In16((BA_R1 + 0x80 * (i + 1) + 4 * j + 2));
			}
			rtime[0][tests_counter + c] = Xil_In32((BA_R1 + 0x80 * (i + 1) + 4 * 20));      //Время переполнения порога
			rtime[1][tests_counter + c] = Xil_In32((u32) (BA_RTC + 0x10));            //Значение счетчика секунд
//          printf("Rcvd: %u sec, %u \n", rtime[1][tests_counter + c], rtime[0][tests_counter + c]);
//          printf("%u tests_counter\n", tests_counter + c);
			c++;
		}
	}

//	for(u32 i = 0; i < check_count; i++)
//		for(u32 j = 0; j < 20; j++)
//		{
//			if(abs(blrci[i][j]) > abs(blrcq[i][j]))
//				iq[tests_counter + i][j] = abs(blrci[i][j]) + abs(blrcq[i][j])/2;
//			else
//				iq[tests_counter + i][j] = abs(blrcq[i][j]) + abs(blrci[i][j])/2;
//		}

	for(u32 i = 0; i < check_count; i++)
		for(u32 j = 0; j < 20; j++)
		{
			iq[tests_counter + i][j]=sqrt(pow(abs(blrci[tests_counter + i][j]),2)+pow(abs(blrcq[tests_counter + i][j]),2));
		}

	for (int i = 0; i < check_count; i++)
	{
		double y[20] = {0};
		for (uint32_t j = 0; j < 20; j++)
			y[j] = iq[tests_counter + i][j];

//		maxes[tests_counter + i] =  parabole_estimation(y,max_ind(iq[tests_counter + i], 20));		// От Ивана
		maxes[tests_counter + i] = approx_parabola2(y, 0, 19);


//		maxes[tests_counter + i] = max_find(iq[tests_counter + i], len);
//		maxes[tests_counter + i] =  max_find_smith(1e-5,blrci[tests_counter + i],blrcq[tests_counter + i]);
//		maxes[tests_counter + i]= (double) max_ind(iq[tests_counter + i], 20);
		maxes[tests_counter + i] += (double)rtime[0][tests_counter + i] - 6.0;
	}
	return 0;
}

/*
 * Проверка на превышение порога и считывание значений регистров
 * uint32_t check_count - количество принимаемых сигналов (пиков)
 */
u32 read_all_corr_buffers(float iq[TESTS_COUNT][20], int32_t rtime[2][TESTS_COUNT], double maxes[TESTS_COUNT], u32 check_count, u32 len)
{
	int16_t blrci[5][20] = {0}, blrcq[5][20] = {0};
	u32 a, c, posb, ni;
	a = c = posb = ni = 0;

	while (c!=check_count)
	{
		a=Xil_In32((BA_R1  + 0x40));  			//Чтение регистра флагов превышения порога
		if (a != 0)
		{
			posb=1;
			for (u32 i=0; i<11; i++)
			{									//Проверка поразрядно регистра флагов прев. порога
				ni = posb & a;					//Для каждого из 11 корреляторов
				if (ni)
				{
					printf(" i = %u \n", i);
					for (u32 j=0; j<20; j++)	//Считывание значений буферных регистров + рег. врем. прев. порога (20)
					{
						blrci[c][j] = Xil_In16((BA_R1 + 0x80 * (i + 1) + 4 * j));		//Тут есть приведение в Xil_In16
						blrcq[c][j] = Xil_In16((BA_R1 + 0x80 * (i + 1) + 4 * j + 2));
					}
					rtime[0][tests_counter + c] = Xil_In32((BA_R1 + 0x80 * (i + 1) + 4 * 20));			//Время переполнения порога
					rtime[1][tests_counter + c] = Xil_In32((u32) (BA_RTC + 0xC));						//Значение счетчика секунд
					printf("%u SEC\n", rtime[1][tests_counter + c]);
					printf("%u tests_counter\n", tests_counter + c);
				}
				posb <<= 1;
			}
			c++;
		}
	}


	for(u32 i = 0; i < check_count; i++)
		for(u32 j = 0; j < 20; j++)
		{
			if(abs(blrci[i][j]) > abs(blrcq[i][j]))
				iq[tests_counter + i][j] = abs(blrci[i][j]) + abs(blrcq[i][j])/2;
			else
				iq[tests_counter + i][j] = abs(blrcq[i][j]) + abs(blrci[i][j])/2;
		}

	for (int i = 0; i < check_count; i++)
		maxes[tests_counter + i] = max_find(iq[tests_counter + i], len);

	return 0;
}

void reset_R1_stat(float iq[TESTS_COUNT][20], int32_t rtime[2][TESTS_COUNT], double maxes[TESTS_COUNT])
{
	dis_delta = dis_deltamax = sum = sum_tr = 0;
	tests_counter = 0;
	for (u32 i = 0; i < TESTS_COUNT; i++)
	{
		maxes[i] = 0;
		rtime[0][i] = 0;
		rtime[1][i] = 0;
		for (u32 j = 0; j < 20; j++)
			iq[i][j] = 0;
	}
}
u32 start_TR1_loop_noTD_fixCount(float iq[TESTS_COUNT][20], int32_t rtime[2][TESTS_COUNT], double maxes[TESTS_COUNT])
{
	reset_R1_stat(iq, rtime, maxes);
	set_initial_timing_conf();
	do {
		start_test_transmission();
//		read_corr_buffer_1(iq, rtime, maxes, PSP_COUNT, (uint32_t) 3);
		tests_counter += PSP_COUNT;
		stop_transmission();
	} while (tests_counter != TESTS_COUNT);

	return 1;
}

u32 calculate_stat_R1(float iq[TESTS_COUNT][20], int32_t rtime[2][TESTS_COUNT], double maxes[TESTS_COUNT])
{
	u32 period = 200000;

	double delta[TESTS_COUNT - 1] = {0};
	double deltamax[TESTS_COUNT - 1] = {0};

//	rtime[1][0] = rtime[1][1] = 0;

	sum = 0.0;
	sum_tr = 0.0;
	//if(rtime[1][0] > rtime[1][1]) rtime[1][0] = 0;
	for(u32 i = 1; i < TESTS_COUNT; i++)
	{
		delta[i-1] = rtime[0][i] - rtime[0][i-1];
		if(rtime[1][i-1] != rtime[1][i])
			delta[i-1] += 20000000;
		deltamax[i-1] = (double)delta[i-1] + maxes[i] - maxes[i-1] - period;
		deltamax[i-1] = maxes[i] - maxes[i-1];
#ifndef TD_MODE
		if(maxes[i] < maxes[i-1])
			deltamax[i-1] += 20000000;
#endif
		///////////////////////////
		sum 	 += deltamax[i-1];
		sum_tr += delta[i-1];
	}
	sum 	 -= deltamax[0];
	sum_tr -= delta[0];
	sum 	 /= TESTS_COUNT - 2;
	sum_tr /= TESTS_COUNT - 2;

	dis_delta  	 = 0.0;
	dis_deltamax = 0.0;
	for(u32 i = 1; i < TESTS_COUNT - 1; i++)
	{
		//////
		if (delta[i] > period + 50 || deltamax[i] > period + 50)
			error_counter++;
		/////////
		dis_delta 	 += pow((delta[i] - sum_tr), 2);
		dis_deltamax += pow((deltamax[i] - sum), 2);
	}
	dis_delta /= TESTS_COUNT - 2;
	dis_delta = sqrt(dis_delta);		//Период между приемами
	dis_deltamax /= TESTS_COUNT - 2;
	dis_deltamax = sqrt(dis_deltamax);	//Макс. период между приемами

	// Отладочное условие для отлавливания выбросов
	if (dis_delta > 2) {
		u32 i = 1; }

	if (dis_delta > 1000) {
		u32 a = 1; }

//	// Cast arrays to watch in debug
//	float iq_stat[TESTS_COUNT][20];
//	int32_t rtime_stat[2][TESTS_COUNT];
//	double maxes_stat[TESTS_COUNT];
//
//	memcpy(iq_stat, iq, sizeof(iq));
//	memcpy(rtime_stat, rtime, sizeof(rtime));
//	memcpy(maxes_stat, maxes, sizeof(maxes));

//	xil_printf("%lf\n", dis_deltamax);


	print_stat(dis_delta, dis_deltamax, sum, sum_tr);

	return 1;
}

void print_stat(double dis_delta, double dis_deltamax, double sum, double sum_tr)
{
/*	xil_printf("[%u.%u, %u.%u, %u.%u, %u.%u]\n",
			get_int_part_from_double(dis_delta), get_float_part_from_double(dis_delta),
			get_int_part_from_double(dis_deltamax), get_float_part_from_double(dis_deltamax),
			get_int_part_from_double(sum), get_float_part_from_double(sum),
			get_int_part_from_double(sum_tr), get_float_part_from_double(sum_tr));*/

//	char dis_delta_char[50], dis_deltamax_char[50], sum_char[50], sum_tr_char[50];
	xil_printf("[");
//	print_double(dis_delta);
//	xil_printf(",");
	print_double(dis_deltamax, 8);
	xil_printf(",");
	print_double(sum, 8);
//	xil_printf(",");
//	print_double(sum_tr);
	xil_printf("]\n");
//	sprintf(dis_deltamax_char, "%2.13f", dis_deltamax);
//	sprintf(sum_char, "%2.13f", sum);
//	sprintf(sum_tr_char, "%2.13f", sum_tr);

//	xil_printf("[%s, %s, %s, %s]\n", dis_delta_char, dis_deltamax_char, sum_char, sum_tr_char);

}

u32 get_int_part_from_double(double d_num)
{
	return floor(d_num);
}

u32 get_float_part_from_double(double d_num)
{
	u32 float_part = 0, int_part = floor(d_num);
	float_part = d_num * pow(10, 8) - int_part * pow(10, 8);
	return float_part;
}

void set_initial_timing_conf()
{
	xil_printf("\tEntered set_initial_timing_conf()...\n");
	u32 a, start_second;
	Xil_Out32((BA_RTC), (u32)(1));			   		// внутр. метки
	Xil_Out32((BA_RTC + 0x4), (u32)(20000000 - 1));	// секунда = 20000000 тактов
	start_second = Xil_In32(BA_RTC + 0x10);
	Xil_Out32((BA_RTC + 0xC), (u32)(0));			// сброс счетчика

//	if (start_second == 0)
//		start_second = 1;
//	else
//		start_second = 0;

//	printf("")

//	start_second = 1;

	u64 i = 0;
	do {
		a = Xil_In32(BA_RTC + 0x10);
//		printf("\t\tSecond counter = %u\n", a);
		mdelay(250);
		i++;
		if (i > 20)
			printf("RTC eternity!\n");
	} while (a == start_second || a == 0);

	xil_printf("\Done set_initial_timing_conf()...\n");
}

void set_initial_timing_conf_with_ext_sync()
{
//	printf("\tEntered set_initial_timing_conf()...\n");
	u32 a, start_second;
	Xil_Out32((BA_RTC + 0xC), (u32)(0));			// сброс счетчика
	Xil_Out32((BA_RTC), (u32)(10));			   		// внутр. метки
	Xil_Out32((BA_RTC + 0x14), (u32)(1));			// Задний фронт
	Xil_Out32((BA_RTC + 0x4), (u32)(20000000 - 1));	// секунда = 20000000 тактов
	start_second = Xil_In32(BA_RTC + 0x10);


//    Xil_Out32((BA_RTC + 0xC), (u32)(0));            // сброс счетчика
//    Xil_Out32((BA_RTC + 0x4), (u32)(20000000 - 1));
////    mdelay(1250);
//    Xil_Out32((BA_RTC), (u32)(10));                       // внутр. метки
//    u32 mode = Xil_In32(BA_RTC);


//	if (start_second == 0)
//		start_second = 1;
//	else
//		start_second = 0;

//	printf("")

//	start_second = 1;
//	xil_printf("Start second = %u\n", start_second);
	xil_printf("Waiting for reset...\n");
	u64 i = 0;
	do {
		a = Xil_In32(BA_RTC + 0x10);
//		printf("\t\tSecond counter = %u\n", a);
//		printf("%u:%u, mode = %u\n", Xil_In32(BA_RTC + 0x10), Xil_In32(BA_RTC + 0x08), Xil_In32(BA_RTC));
//		mdelay(500);
//		i++;
//		if (i > 20)
//			printf("RTC eternity!\n");
	} while (a == start_second);
	xil_printf("Reset success!\n");
//	set_initial_timing_conf();
}

//void set_initial_timing_conf()
//{
//     u32 a, start_second;
//     Xil_Out32((BA_RTC), (u32)(1));                       // внутр. метки
//     Xil_Out32((BA_RTC + 0x4), (u32)(20000000 - 1));    // секунда =20000000 тактов
//     start_second = Xil_In32(BA_RTC + 0x10);
//     Xil_Out32((BA_RTC + 0xC), (u32)(0));            // сброс счетчика
//
//     //if (start_second == 0)
//     //    start_second = 1;
//     //else
//         start_second = 1;
//
//     u64 i = 0;
//
//     do {
//         a = Xil_In32(BA_RTC + 0x10);
//         i++;
//         if (i > 100000000) {
//             printf("RTC eternity!\n");
//             mdelay(500);
//             u32 sek = Xil_In32(BA_RTC + 0x10);
//             u32 initsek = Xil_In32(BA_RTC + 0xC);
//             printf("sec = %u initsek = %u\n", sek, initsek);
//         }
//     } while (a != start_second);
//}

u32 check_etalon(u32 etalon[32])
{
	for (u32 i = 0; i < 32; i++)
	{
//		etalon[i] = Xil_In32(BA_ETALON + (u32)(i * 4));
//		xil_printf("%u ", etalon[i]);
	}
//	xil_printf("\n");
	return 0;
}

u32 psp_scan_init()
{
	/*
	 * Блок заполнения памяти сканера ПСП
	 */
	for(u32 i = 0; i < 32768; i ++)
			Xil_Out32((BA_mem + i*4),(u32)i);

	//-------------SCANER_MEMORY_INIT---------------------
	Xil_Out32((BA_GPSP),        (u32)(0));
	Xil_Out32((BA_GPSP + 0x04), (u32)(0));
	Xil_Out32((BA_GPSP + 0x08), (u32)(0));
	Xil_Out32((BA_GPSP + 0x0C), (u32)(0));
	Xil_Out32((BA_GPSP + 0x10), (u32)(0));
	Xil_Out32((BA_GPSP + 0x14), (u32)(0));
	Xil_Out32((BA_GPSP + 0x18), (u32)(0));
	Xil_Out32((BA_GPSP + 0x1C), (u32)(0));
	Xil_Out32((BA_GPSP + 0x20), (u32)(0));
	Xil_Out32((BA_GPSP + 0x24), (u32)(0x204));
	Xil_Out32((BA_GPSP + 0x28), (u32)(0x3ff));
	Xil_Out32((BA_GPSP + 0x2C), (u32)(0));
	Xil_Out32((BA_GPSP + 0x30), (u32)(0));
	Xil_Out32((BA_GPSP + 0x34), (u32)(0));
	Xil_Out32((BA_GPSP + 0x38), (u32)(0));
	Xil_Out32((BA_GPSP + 0x3C), (u32)(0));
	Xil_Out32((BA_GPSP + 0x40), (u32)(0));
	Xil_Out32((BA_GPSP + 0x44), (u32)(0));
	Xil_Out32((BA_GPSP + 0x48), (u32)(0));
	Xil_Out32((BA_GPSP + 0x4C), (u32)(0));
	Xil_Out32((BA_GPSP + 0x50), (u32)(0));
	Xil_Out32((BA_GPSP + 0x54), (u32)(0));
	Xil_Out32((BA_GPSP + 0x58), (u32)(0));
	Xil_Out32((BA_GPSP + 0x5C), (u32)(0));
	Xil_Out32((BA_GPSP + 0x60), (u32)(0));
	Xil_Out32((BA_GPSP + 0x64), (u32)(0));
	Xil_Out32((BA_GPSP + 0x68), (u32)(0));
	Xil_Out32((BA_GPSP + 0x6C), (u32)(0));
	Xil_Out32((BA_GPSP + 0x70), (u32)(0));
	Xil_Out32((BA_GPSP + 0x74), (u32)(0));
	Xil_Out32((BA_GPSP + 0x78), (u32)(0));
	Xil_Out32((BA_GPSP + 0x7C), (u32)(0));
	Xil_Out32((BA_GPSP + 0x80), (u32)(0));
	Xil_Out32((BA_GPSP + 0x84), (u32)(0));
	Xil_Out32((BA_GPSP + 0x88), (u32)(0));
	Xil_Out32((BA_GPSP + 0x8C), (u32)(0));
	Xil_Out32((BA_GPSP + 0x90), (u32)(0));

	Xil_Out32((BA_GPSP),        (u32)(0x01));		//Регистр управления control_reg

	Xil_Out32((BA_RC + 0x14), (u32)(0x01));			//Включ. такт. частоты генератора ПСП

	u32 b;
	for (u32 i=0; i<10000; i++) b=Xil_In32(BA_T1);	//Задержка

//	mdelay(500);

	return 0;
}

u32 start_delayed_transmission(u32 start_time)
{
//	stop_transmission();

	//Блок заполнения памяти сканера ПСП

//	for(u32 i = 0; i < 32768; i ++)
//			Xil_Out32((BA_mem + i*4),(u32)0);
//
//	//-------------SCANER_MEMORY_INIT---------------------
//	Xil_Out32((BA_GPSP),        (u32)(0));
//	Xil_Out32((BA_GPSP + 0x04), (u32)(0));
//	Xil_Out32((BA_GPSP + 0x08), (u32)(0));
//	Xil_Out32((BA_GPSP + 0x0C), (u32)(0));
//	Xil_Out32((BA_GPSP + 0x10), (u32)(0));
//	Xil_Out32((BA_GPSP + 0x14), (u32)(0));
//	Xil_Out32((BA_GPSP + 0x18), (u32)(0));
//	Xil_Out32((BA_GPSP + 0x1C), (u32)(0));
//	Xil_Out32((BA_GPSP + 0x20), (u32)(0));
//	Xil_Out32((BA_GPSP + 0x24), (u32)(0x204));
//	Xil_Out32((BA_GPSP + 0x28), (u32)(0x3FF));
//	Xil_Out32((BA_GPSP + 0x2C), (u32)(0));
//	Xil_Out32((BA_GPSP + 0x30), (u32)(0));
//	Xil_Out32((BA_GPSP + 0x34), (u32)(0));
//	Xil_Out32((BA_GPSP + 0x38), (u32)(0));
//	Xil_Out32((BA_GPSP + 0x3C), (u32)(0));
//	Xil_Out32((BA_GPSP + 0x40), (u32)(0));
//	Xil_Out32((BA_GPSP + 0x44), (u32)(0));
//	Xil_Out32((BA_GPSP + 0x48), (u32)(0));
//	Xil_Out32((BA_GPSP + 0x4C), (u32)(0));
//	Xil_Out32((BA_GPSP + 0x50), (u32)(0));
//	Xil_Out32((BA_GPSP + 0x54), (u32)(0));
//	Xil_Out32((BA_GPSP + 0x58), (u32)(0));
//	Xil_Out32((BA_GPSP + 0x5C), (u32)(0));
//	Xil_Out32((BA_GPSP + 0x60), (u32)(0));
//	Xil_Out32((BA_GPSP + 0x64), (u32)(0));
//	Xil_Out32((BA_GPSP + 0x68), (u32)(0));
//	Xil_Out32((BA_GPSP + 0x6C), (u32)(0));
//	Xil_Out32((BA_GPSP + 0x70), (u32)(0));
//	Xil_Out32((BA_GPSP + 0x74), (u32)(0));
//	Xil_Out32((BA_GPSP + 0x78), (u32)(0));
//	Xil_Out32((BA_GPSP + 0x7C), (u32)(0));
//	Xil_Out32((BA_GPSP + 0x80), (u32)(0));
//	Xil_Out32((BA_GPSP + 0x84), (u32)(0));
//	Xil_Out32((BA_GPSP + 0x88), (u32)(0));
//	Xil_Out32((BA_GPSP + 0x8C), (u32)(0));
//	Xil_Out32((BA_GPSP + 0x90), (u32)(0));
//
//	Xil_Out32((BA_GPSP),        (u32)(0x01));		//Регистр управления control_reg
//
//	Xil_Out32((BA_RC + 0x14), (u32)(0x01));			//Включ. такт. частоты генератора ПСП

	Xil_Out32((BA_T1),        (u32)(0x0000000));	//Посылается 0 бит данных (2002 для посылки другого к-ва) 0x00000C02 0x00000C06
	Xil_Out32((BA_T1 + 0x04), (u32)(0x000000D9));	//Регистр данных для передачи 1
	Xil_Out32((BA_T1 + 0x04), (u32)(0xFFFFFFFF));	//Регистр данных для передачи 2
	Xil_Out32((BA_T1 + 0x10), (u32)(429497));		//Код шага частоты модуляции
	Xil_Out32((BA_T1 + 0x14), (u32)(start_time));		//Задержка period

//	xil_printf("CURRENT_TIME = %u, %u\t\tNEXT_START_TIME = %u\n", Xil_In32((u32) (BA_RTC + 0x10)), Xil_In32((u32) (BA_RTC + 0x08)), start_time);

//	u32 b;
//	for (u32 i=0; i<10000; i++) b=Xil_In32(BA_T1);	//Задержка

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//Настройка приёмника
	Xil_Out32((BA_R1 + 0x14), (u32)(3000));			//Установка уровня порога
	Xil_Out32((BA_R1 + 0x18), (u32)(4));			//Номер регистра для сравнения с порогом

	u32 cod = (u32)(pow(2, 32)*50) / 10e6;
//	Xil_Out32((BA_R1), (u32)(cod));				//DDS1
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	Xil_Out32((BA_RC), (u32)(0x03));				//Обнуление приемника и передатчика 1 канала

	Xil_Out32((BA_T1 + 0x0C), (u32)(0x00000002));	//Включение передатчика

	return 1;
}

/*u32 start_T1_with_TD(float iq[TESTS_COUNT][20], int32_t rtime[2][TESTS_COUNT], double maxes[TESTS_COUNT], u32 period)
{
	set_initial_timing_conf();
//	start_td_psp_transmission_halfsec();
	psp_scan_init();
	start_td_psp_transmission(iq, rtime, maxes, period, 0);	//Каждые 10 мсек = 200000
	stop_transmission();

	return 1;
}*/

u32 start_T1_with_TD(u32 period, u32 start_slot)
{
	stop_transmission();

//	set_initial_timing_conf();
//	set_initial_timing_conf_with_ext_sync(); // Для слотов в режиме с меткой

	psp_scan_init();

//	char c = 0;
//	xil_printf("Waiting for command to start Transmission...\n");
//	scanf("%c", &c);
//	xil_printf("YES, my Commander\n");

	while (1)
	{
		start_td_psp_transmission(period, start_slot, 0);	//Каждые 10 мсек = 200000
	}
	return 1;
}

u32 start_TR1_loop_with_TD(float iq[TESTS_COUNT][20], int32_t rtime[2][TESTS_COUNT], double maxes[TESTS_COUNT], u32 period)
{
	stop_transmission();
	reset_R1_stat(iq, rtime, maxes);
//	start_td_psp_transmission(iq, rtime, maxes, period, 1);
	stop_transmission();
	calculate_stat_R1(iq, rtime, maxes);
//		tests_counter--;
//		read_corr_buffer_1(iq, rtime, maxes, PSP_COUNT, (uint32_t) 8);
	return 1;
}

void start_T1_noTD()
{
	do {
		start_test_transmission();
		mdelay(3);
		stop_transmission();
		} while (1);
}

void start_T1_constantly()
{
	psp_scan_init();
//	Xil_Out32((BA_mem),        (u32)(0xacd6ea65));
//	Xil_Out32((BA_mem + 0x04), (u32)(0xb2bec7e0));
//	Xil_Out32((BA_mem + 0x08), (u32)(0xbc4ef0f5));
//	Xil_Out32((BA_mem + 0x0C), (u32)(0x69a69432));
//	Xil_Out32((BA_mem + 0x10), (u32)(0x8c809f98));
//	Xil_Out32((BA_mem + 0x14), (u32)(0xcc249e6d));
//	Xil_Out32((BA_mem + 0x18), (u32)(0x7b27127f));
//	Xil_Out32((BA_mem + 0x1C), (u32)(0x8943fb26));
//	Xil_Out32((BA_mem + 0x20), (u32)(0xc20e01f9));
//	Xil_Out32((BA_mem + 0x24), (u32)(0xc8e136cf));
//	Xil_Out32((BA_mem + 0x28), (u32)(0xbdbb3ced));
//	Xil_Out32((BA_mem + 0x2C), (u32)(0x0b0753b3));
//	Xil_Out32((BA_mem + 0x30), (u32)(0x697b98d0));
//	Xil_Out32((BA_mem + 0x34), (u32)(0xd48194ec));
//	Xil_Out32((BA_mem + 0x38), (u32)(0x16f99937));
//	Xil_Out32((BA_mem + 0x3C), (u32)(0x8e4ff10b));
//	Xil_Out32((BA_mem + 0x40), (u32)(0x851806dc));
//	Xil_Out32((BA_mem + 0x44), (u32)(0x45c43371));
//	Xil_Out32((BA_mem + 0x48), (u32)(0x74636eec));
//	Xil_Out32((BA_mem + 0x4C), (u32)(0x3b59b6a5));
//	Xil_Out32((BA_mem + 0x50), (u32)(0x2cd489c5));
//	Xil_Out32((BA_mem + 0x54), (u32)(0xe49d5233));
//	Xil_Out32((BA_mem + 0x58), (u32)(0xf90cc669));
//	Xil_Out32((BA_mem + 0x5C), (u32)(0xeb295c50));
//	Xil_Out32((BA_mem + 0x60), (u32)(0x5da7e865));
//	Xil_Out32((BA_mem + 0x64), (u32)(0x88c825ac));
//	Xil_Out32((BA_mem + 0x68), (u32)(0x801e79cb));
//	Xil_Out32((BA_mem + 0x6C), (u32)(0xcf03e65c));
//	Xil_Out32((BA_mem + 0x70), (u32)(0x93d42610));
//	Xil_Out32((BA_mem + 0x74), (u32)(0xcc667e4c));
//	Xil_Out32((BA_mem + 0x78), (u32)(0x2c10ce82));
//	Xil_Out32((BA_mem + 0x7C), (u32)(0x37cc30ca));
//
//	Xil_Out32((BA_GPSP + 0x5C), (u32)(0x01));
//	Xil_Out32((BA_GPSP + 0x68), (u32)(0x01));
//	Xil_Out32((BA_GPSP),        (u32)(0x01));
//
//	Xil_Out32((BA_RC + 0x14), (u32)(0x01));

	Xil_Out32((BA_T1),        (u32)(0x00022090));
	Xil_Out32((BA_T1 + 0x04), (u32)(0x000000D9));
	Xil_Out32((BA_T1 + 0x10), (u32)(429497));

	u32 b = 0;
	for (u32 i=0; i<10000; i++) b=Xil_In32(BA_T1);

	Xil_Out32((BA_R1 + 0x14), (u32)(5000));
	Xil_Out32((BA_R1 + 0x18), (u32)(4));

	Xil_Out32((BA_RC), (u32)(0x03)); //Transmitter
	Xil_Out32((BA_T1 + 0xc), (u32)(1));

	while (1) {};
}

u32 read_I_and_Q_corrs()
{
	u32 I[320] = {0}, Q[320] = {0};

	u32 BA_ICORR = 0x43C01000;
	u32 BA_QCORR = 0x43C01800;

	for (u32 i = 0; i < 320; i++)
	{
		I[i] = Xil_In32(BA_ICORR + (u32)(i * 4));
		Q[i] = Xil_In32(BA_QCORR + (u32)(i * 4));
	}

	u32 etalon[32] = {0};
	check_etalon(etalon);
	return 0;
}

void ch1_fir_tr_load()
{
		uint32_t uu=0;
		uu= (u32)-4;

		Xil_Out32((BA_T1k1 + 0x00), (u32)(-3));
		Xil_Out32((BA_T1k1 + 0x04), (u32)(-4  ));
		Xil_Out32((BA_T1k1 + 0x08), (u32)(-4  ));
		Xil_Out32((BA_T1k1 + 0x0C), (u32)(-4  ));
		Xil_Out32((BA_T1k1 + 0x10), (u32)(-2  ));
		Xil_Out32((BA_T1k1 + 0x14), (u32)(1  ));
		Xil_Out32((BA_T1k1 + 0x18), (u32)(6  ));
		Xil_Out32((BA_T1k1 + 0x1C), (u32)(13  ));
		Xil_Out32((BA_T1k1 + 0x20), (u32)(20  ));
		Xil_Out32((BA_T1k1 + 0x24), (u32)(27  ));
		Xil_Out32((BA_T1k1 + 0x28), (u32)(32  ));
		Xil_Out32((BA_T1k1 + 0x2C), (u32)(32  ));
		Xil_Out32((BA_T1k1 + 0x30), (u32)(27  ));
		Xil_Out32((BA_T1k1 + 0x34), (u32)(15  ));
		Xil_Out32((BA_T1k1 + 0x38), (u32)(-3  ));
		Xil_Out32((BA_T1k1 + 0x3C), (u32)(-29  ));
		Xil_Out32((BA_T1k1 + 0x40), (u32)(-58  ));
		Xil_Out32((BA_T1k1 + 0x44), (u32)(-88  ));
		Xil_Out32((BA_T1k1 + 0x48), (u32)(-113  ));
		Xil_Out32((BA_T1k1 + 0x4C), (u32)(-129  ));
		Xil_Out32((BA_T1k1 + 0x50), (u32)(-128  ));
		Xil_Out32((BA_T1k1 + 0x54), (u32)(-108  ));
		Xil_Out32((BA_T1k1 + 0x58), (u32)(-63  ));
		Xil_Out32((BA_T1k1 + 0x5C), (u32)(7  ));
		Xil_Out32((BA_T1k1 + 0x60), (u32)(102  ));
		Xil_Out32((BA_T1k1 + 0x64), (u32)(218  ));
		Xil_Out32((BA_T1k1 + 0x68), (u32)(349  ));
		Xil_Out32((BA_T1k1 + 0x6C), (u32)(485  ));
		Xil_Out32((BA_T1k1 + 0x70), (u32)(616  ));
		Xil_Out32((BA_T1k1 + 0x74), (u32)(732  ));
		Xil_Out32((BA_T1k1 + 0x78), (u32)(823  ));
		Xil_Out32((BA_T1k1 + 0x7C), (u32)(881  ));
		Xil_Out32((BA_T1k1 + 0x80), (u32)(901  ));



}

void ch1_fir_rec_load()
{
//	Xil_Out32((BA_R1k1 + 0x00), (u32)(-73	));
//	Xil_Out32((BA_R1k1 + 0x04), (u32)(47    ));
//	Xil_Out32((BA_R1k1 + 0x08), (u32)(-0    ));
//	Xil_Out32((BA_R1k1 + 0x0C), (u32)(-50   ));
//	Xil_Out32((BA_R1k1 + 0x10), (u32)(85    ));
//	Xil_Out32((BA_R1k1 + 0x14), (u32)(-88   ));
//	Xil_Out32((BA_R1k1 + 0x18), (u32)(57    ));
//	Xil_Out32((BA_R1k1 + 0x1C), (u32)(-0    ));
//	Xil_Out32((BA_R1k1 + 0x20), (u32)(-62   ));
//	Xil_Out32((BA_R1k1 + 0x24), (u32)(105   ));
//	Xil_Out32((BA_R1k1 + 0x28), (u32)(-110  ));
//	Xil_Out32((BA_R1k1 + 0x2C), (u32)(71    ));
//	Xil_Out32((BA_R1k1 + 0x30), (u32)(-0    ));
//	Xil_Out32((BA_R1k1 + 0x34), (u32)(-79   ));
//	Xil_Out32((BA_R1k1 + 0x38), (u32)(136   ));
//	Xil_Out32((BA_R1k1 + 0x3C), (u32)(-144  ));
//	Xil_Out32((BA_R1k1 + 0x40), (u32)(95    ));
//	Xil_Out32((BA_R1k1 + 0x44), (u32)(-0    ));
//	Xil_Out32((BA_R1k1 + 0x48), (u32)(-109  ));
//	Xil_Out32((BA_R1k1 + 0x4C), (u32)(190   ));
//	Xil_Out32((BA_R1k1 + 0x50), (u32)(-206  ));
//	Xil_Out32((BA_R1k1 + 0x54), (u32)(139   ));
//	Xil_Out32((BA_R1k1 + 0x58), (u32)(-0    ));
//	Xil_Out32((BA_R1k1 + 0x5C), (u32)(-171  ));
//	Xil_Out32((BA_R1k1 + 0x60), (u32)(311   ));
//	Xil_Out32((BA_R1k1 + 0x64), (u32)(-356  ));
//	Xil_Out32((BA_R1k1 + 0x68), (u32)(257   ));
//	Xil_Out32((BA_R1k1 + 0x6C), (u32)(-0    ));
//	Xil_Out32((BA_R1k1 + 0x70), (u32)(-386  ));
//	Xil_Out32((BA_R1k1 + 0x74), (u32)(834   ));
//	Xil_Out32((BA_R1k1 + 0x78), (u32)(-1251 ));
//	Xil_Out32((BA_R1k1 + 0x7C), (u32)(1547  ));
//	Xil_Out32((BA_R1k1 + 0x80), (u32)(6617  ));

	/*КИХ фильтр с частотой срез 2.5 МГц*/
//	Xil_Out32((BA_R1k1 + 0x00), (u32)(-0	));
//	Xil_Out32((BA_R1k1 + 0x04), (u32)(-2	));
//	Xil_Out32((BA_R1k1 + 0x08), (u32)(-5	));
//	Xil_Out32((BA_R1k1 + 0x0C), (u32)(-5	));
//	Xil_Out32((BA_R1k1 + 0x10), (u32)(0		));
//	Xil_Out32((BA_R1k1 + 0x14), (u32)(8		));
//	Xil_Out32((BA_R1k1 + 0x18), (u32)(15	));
//	Xil_Out32((BA_R1k1 + 0x1C), (u32)(13	));
//	Xil_Out32((BA_R1k1 + 0x20), (u32)(-0	));
//	Xil_Out32((BA_R1k1 + 0x24), (u32)(-19	));
//	Xil_Out32((BA_R1k1 + 0x28), (u32)(-32	));
//	Xil_Out32((BA_R1k1 + 0x2C), (u32)(-26	));
//	Xil_Out32((BA_R1k1 + 0x30), (u32)(0		));
//	Xil_Out32((BA_R1k1 + 0x34), (u32)(37	));
//	Xil_Out32((BA_R1k1 + 0x38), (u32)(60	));
//	Xil_Out32((BA_R1k1 + 0x3C), (u32)(49	));
//	Xil_Out32((BA_R1k1 + 0x40), (u32)(-0	));
//	Xil_Out32((BA_R1k1 + 0x44), (u32)(-66	));
//	Xil_Out32((BA_R1k1 + 0x48), (u32)(-107 	));
//	Xil_Out32((BA_R1k1 + 0x4C), (u32)(-87	));
//	Xil_Out32((BA_R1k1 + 0x50), (u32)(0		));
//	Xil_Out32((BA_R1k1 + 0x54), (u32)(115	));
//	Xil_Out32((BA_R1k1 + 0x58), (u32)(187	));
//	Xil_Out32((BA_R1k1 + 0x5C), (u32)(154	));
//	Xil_Out32((BA_R1k1 + 0x60), (u32)(-0	));
//	Xil_Out32((BA_R1k1 + 0x64), (u32)(-212 	));
//	Xil_Out32((BA_R1k1 + 0x68), (u32)(-361	));
//	Xil_Out32((BA_R1k1 + 0x6C), (u32)(-314 	));
//	Xil_Out32((BA_R1k1 + 0x70), (u32)(0		));
//	Xil_Out32((BA_R1k1 + 0x74), (u32)(542	));
//	Xil_Out32((BA_R1k1 + 0x78), (u32)(1164 	));
//	Xil_Out32((BA_R1k1 + 0x7C), (u32)(1657 	));
//	Xil_Out32((BA_R1k1 + 0x80), (u32)(1844 	));

//	/*КИХ фильтр с частотой срез 4 МГц*/
//	Xil_Out32((BA_R1k1 + 0x00), (u32)(0				));
//	Xil_Out32((BA_R1k1 + 0x04), (u32)(0             ));
//	Xil_Out32((BA_R1k1 + 0x08), (u32)(-0            ));
//	Xil_Out32((BA_R1k1 + 0x0C), (u32)(-1            ));
//	Xil_Out32((BA_R1k1 + 0x10), (u32)(-0            ));
//	Xil_Out32((BA_R1k1 + 0x14), (u32)(0             ));
//	Xil_Out32((BA_R1k1 + 0x18), (u32)(0             ));
//	Xil_Out32((BA_R1k1 + 0x1C), (u32)(0             ));
//	Xil_Out32((BA_R1k1 + 0x20), (u32)(0             ));
//	Xil_Out32((BA_R1k1 + 0x24), (u32)(1	            ));
//	Xil_Out32((BA_R1k1 + 0x28), (u32)(-1	        ));
//	Xil_Out32((BA_R1k1 + 0x2C), (u32)(-4	        ));
//	Xil_Out32((BA_R1k1 + 0x30), (u32)(0             ));
//	Xil_Out32((BA_R1k1 + 0x34), (u32)(6             ));
//	Xil_Out32((BA_R1k1 + 0x38), (u32)(4             ));
//	Xil_Out32((BA_R1k1 + 0x3C), (u32)(-3            ));
//	Xil_Out32((BA_R1k1 + 0x40), (u32)(-3            ));
//	Xil_Out32((BA_R1k1 + 0x44), (u32)(-0	        ));
//	Xil_Out32((BA_R1k1 + 0x48), (u32)(-6 	        ));
//	Xil_Out32((BA_R1k1 + 0x4C), (u32)(-9	        ));
//	Xil_Out32((BA_R1k1 + 0x50), (u32)(17            ));
//	Xil_Out32((BA_R1k1 + 0x54), (u32)(46	        ));
//	Xil_Out32((BA_R1k1 + 0x58), (u32)(-0	        ));
//	Xil_Out32((BA_R1k1 + 0x5C), (u32)(-99	        ));
//	Xil_Out32((BA_R1k1 + 0x60), (u32)(-85           ));
//	Xil_Out32((BA_R1k1 + 0x64), (u32)(116 	        ));
//	Xil_Out32((BA_R1k1 + 0x68), (u32)(254	        ));
//	Xil_Out32((BA_R1k1 + 0x6C), (u32)(-0 	        ));
//	Xil_Out32((BA_R1k1 + 0x70), (u32)(-473          ));
//	Xil_Out32((BA_R1k1 + 0x74), (u32)(-419	        ));
//	Xil_Out32((BA_R1k1 + 0x78), (u32)(662 	        ));
//	Xil_Out32((BA_R1k1 + 0x7C), (u32)(2209 	        ));
//	Xil_Out32((BA_R1k1 + 0x80), (u32)(2949 	        ));

	/*20_mhz_2_5_cut*/
	Xil_Out32((BA_R1k1+0x0),(u32)(0		));
	Xil_Out32((BA_R1k1+0x4),(u32)(15		));
	Xil_Out32((BA_R1k1+0x8),(u32)(23		));
	Xil_Out32((BA_R1k1+0xc),(u32)(16		));
	Xil_Out32((BA_R1k1+0x10),(u32)(0		));
	Xil_Out32((BA_R1k1+0x14),(u32)(-13		));
	Xil_Out32((BA_R1k1+0x18),(u32)(-15		));
	Xil_Out32((BA_R1k1+0x1c),(u32)(-6		));
	Xil_Out32((BA_R1k1+0x20),(u32)(0		));
	Xil_Out32((BA_R1k1+0x24),(u32)(-8		));
	Xil_Out32((BA_R1k1+0x28),(u32)(-26		));
	Xil_Out32((BA_R1k1+0x2c),(u32)(-32		));
	Xil_Out32((BA_R1k1+0x30),(u32)(0		));
	Xil_Out32((BA_R1k1+0x34),(u32)(68		));
	Xil_Out32((BA_R1k1+0x38),(u32)(129		));
	Xil_Out32((BA_R1k1+0x3c),(u32)(118		));
	Xil_Out32((BA_R1k1+0x40),(u32)(0		));
	Xil_Out32((BA_R1k1+0x44),(u32)(-189		));
	Xil_Out32((BA_R1k1+0x48),(u32)(-329		));
	Xil_Out32((BA_R1k1+0x4c),(u32)(-284		));
	Xil_Out32((BA_R1k1+0x50),(u32)(0		));
	Xil_Out32((BA_R1k1+0x54),(u32)(415		));
	Xil_Out32((BA_R1k1+0x58),(u32)(705		));
	Xil_Out32((BA_R1k1+0x5c),(u32)(598		));
	Xil_Out32((BA_R1k1+0x60),(u32)(0		));
	Xil_Out32((BA_R1k1+0x64),(u32)(-874		));
	Xil_Out32((BA_R1k1+0x68),(u32)(-1517		));
	Xil_Out32((BA_R1k1+0x6c),(u32)(-1342		));
	Xil_Out32((BA_R1k1+0x70),(u32)(0		));
	Xil_Out32((BA_R1k1+0x74),(u32)(2377		));
	Xil_Out32((BA_R1k1+0x78),(u32)(5137		));
	Xil_Out32((BA_R1k1+0x7c),(u32)(7347		));
	Xil_Out32((BA_R1k1+0x80),(u32)(8191		));


}

u32 get_time_slot_id(double corr_max)
{
	u32 i = 0;
	u32 corr_max_int = (u32)corr_max;
	u32 period = 20000000 / SLOT_COUNT;
	u32 slot_map[SLOT_COUNT + 1] = {0};

	for (i = 0; i <= SLOT_COUNT; i++)
		slot_map[i] = i * period;

	for (i = 0; i <= SLOT_COUNT; i++)
	{

		if (corr_max_int < slot_map[i])
			return i - 1;
	}

	return 666;
}

u32 enable_main_transmission_cycle()
{
	xil_printf("1st channel Transmission started...\n");
	//enable_1st_channel_analog_transmission(1400000000, 2000000000, 0);

	// Для СЛОТОВ
	u32 period = 20000000 / SLOT_COUNT;

	//rppu_1ch_transmission_enable( rppu_rf_phy,(uint32_t) (145000000));
	// rppu_sint1_set_freq_alex( );
	/*rppu_VS_edge_detection( rppu_rf_phy);*/
	//rppu_band_edge_detection(rppu_rf_phy,0,&lower_LC,10,10);

	ch1_fir_tr_load();


//	start_T1_with_TD(period, START_SLOT);
//	set_initial_timing_conf();
	start_T1_constantly();

	stop_transmission();

	//start_T1_noTD();
//						start_T1_with_TD(iq, rtime, maxes, 500000);
	return 0;
}

u32 enable_main_recv_cycle()
{
	float iq[TESTS_COUNT][20] = {0};
	int32_t rtime[2][TESTS_COUNT] = {0};
	double maxes[TESTS_COUNT] = {0};
	int16_t blrci[TESTS_COUNT][20] = {0}, blrcq[TESTS_COUNT][20] = {0};
	double td_maxes[TESTS_COUNT] = {0};
	double pure_maxes[TESTS_COUNT] = {0};
	u32 grand_test_counter = 0;

	u32 run_counter = 0;
	u32 ret = 0;

#ifdef CALIBRATION
	for (u32 i = 0; i < 5; i ++)
		calibrated_2_meters[i] = 0;
#endif

//	#ifdef TD_MODE
//							xil_printf("1st channel Receiving started in TD_MODE...\n");
//							set_initial_timing_conf_with_ext_sync();
//	#else
//							xil_printf("1st channel Receiving started...\n");
//							set_initial_timing_conf();
//	#endif
	enable_cnt_i_corr = enable_cnt_q_corr =  1;
	cnt_q_corr = cnt_i_corr = 0;

	u32 temp = 0;
//						rppu_enable_test(rppu_rf_phy,(uint32_t) (145000000));

	while (1)
	{

		double max_ind_val_sum = 0;
		u32 cal_stat = 1;

		//enable_1st_channel_analog_recv((u32)(2 * GHz), 32);
//							enable_1st_channel_analog_recv((u32)(2 * GHz), 76);
//							ad9361_set_rx_rfdc_track_en_dis (ad9361_phy, ENABLE);
//							ad9361_set_rx_bbdc_track_en_dis (ad9361_phy, DISABLE);
//							ad9361_set_rx_quad_track_en_dis (ad9361_phy, ENABLE);

		//rppu_1ch_reciev_enable( rppu_rf_phy,(uint32_t) (145000000));
//							rppu_lna_disable(rppu_rf_phy);

//							rppu_1ch_reciev_disable( rppu_rf_phy,(uint32_t) (145000000));

//							rppu_1ch_reciev_enable( rppu_rf_phy,(uint32_t) (145000000));
//							rppu_1ch_dc_corr(rppu_rf_phy);

		//ad9361_set_tx_auto_cal_en_dis (ad9361_phy, ENABLE);


//		write_one_byte_spi( 0x171, (u32) 246);
//
//		get_reg_state();

		stop_transmission();

		ch1_fir_rec_load();

		run_counter++;
//							xil_printf("\t\t\t\tRUN NUBMER %u\n", run_counter);
		grand_test_counter = 0;
//		ad9361_do_calib(ad9361_phy,  RFDC_CAL, 0);
		while (grand_test_counter < GRAND_TESTS_COUNT)
		{
//			ad9361_do_calib(ad9361_phy,  RFDC_CAL, 0);
//			ad9361_do_calib(ad9361_phy,  TX_QUAD_CAL, 0);
#ifndef TD_MODE
			reset_R1_stat(iq, rtime, maxes);
#else
			reset_R1_stat(iq, rtime, td_maxes);
#endif
			start_test_rcv((uint32_t) 3500, (uint32_t) 3);

//								mdelay(1000);
//								read_and_print_DC_lvl();
//			enable_DC_correction_nodriver();

//								if (measure_and_calc_correct_i_q_levels())
//									break;
//								while(1)
//								{
//									mdelay(1000);
//									read_and_print_DC_lvl();
//								}
//								IFA_Change();


//								while (1)
//									check_input_power();

//								check_input_power();

//								temp++;
//								mdelay(500);
//								if (temp < 16)
//									break;

//								enable_dc_correction(rppu_rf_phy);
#ifdef CALIBRATION

			char ch1;
#ifdef METHOD_NUM
			if (calibrated_2_meters[METHOD_NUM] == 0)
#else
			if (calibrated_2_meters[0] == 0)
#endif
			{
				xil_printf("Waiting for 2 meters calibration...\n");
//									scanf("%c%*c", &ch1);
				xil_printf("2 meters calibrated!\n");
			}

#endif

			do {
//									check_etalon();
//									ret = read_corr_buffer_1(iq, rtime, maxes, blrci, blrcq, PSP_COUNT, (uint32_t) 8);
				ret = read_specific_corr_buffer(0, iq, rtime, maxes, blrci, blrcq, PSP_COUNT, (uint32_t) 8);

				if (maxes[tests_counter] != 0)
				{
//										xil_printf("That slot is %u, max = ", get_time_slot_id(maxes[tests_counter]));
//					printf("%lf\n", maxes[tests_counter] - get_time_slot_id(maxes[tests_counter]) * 500000);
				}

//				if (ret == 1)
//					break;
				tests_counter += PSP_COUNT;
//								printf("tests_counter = %u\n", tests_counter);
			} while (tests_counter != TESTS_COUNT);


#ifdef METHOD_NUM
			u32 max_counter = METHOD_NUM;
#else
			for (u32 max_counter = 0; max_counter < 6; max_counter++)
#endif
			{


				double max_value_sum = 0, max_value_dis = 0;
				u32 filtered_count = 0;
				double max_ind_val_sum_temp = 0;

				for (u32 tc = 0; tc < TESTS_COUNT; tc++)
				{
					double y[20] = {0};
					double x[20] = {0};
					double max_ind_val = iq[tc][max_ind(iq[tc], 20)];
					double max_value = 0;

					if (max_ind_val > 0 /*max_ind_val_sum*/)
					{
						switch(max_counter)
						{
						case 0:
							max_value = max_find(iq[tc], 8);

					//		maxes[tests_counter + i]= (double) max_ind(iq[testbhs_counter + i], 20);

	//						printf("[%lf]", maxes[tests_counter + i]);
							break;

						case 1:
							if(iq[tc][max_ind(iq[tc], 20)] > 0)
							{
								double i_arr[20] = {0};
								double q_arr[20] = {0};

								for (u32 j = 0; j < 20; j ++)
								{
									i_arr[j] = blrci[tc][j];
									q_arr[j] = blrcq[tc][j];
								}

								max_value = find_est_max(i_arr, q_arr);
				//				printf("[%lf]", maxes[tests_counter + i]);
							}
							else
								max_value = 0;
							break;

						case 2:
							for (uint32_t j = 6; j < 13; j++)
								y[j-6 ] = iq[tc][j];

							least_squares(y, 7);

							max_value = -kk[1] / (2*kk[2]) + 6;
				//			printf("[%lf]", maxes[tests_counter + i]);
							break;

						case 3:
							for (uint32_t j = 0; j < 20; j++)
								y[j] = iq[tc][j];

							max_value =  parabole_estimation(y,max_ind(iq[tc], 20));		// От Ивана
				//			printf("[%lf]", maxes[tests_counter + i]);
							break;
						case 4:
							for (uint32_t j = 0; j < 20; j++)
								y[j] = iq[tc][j];

							max_value = approx_parabola2(y, 0, 19);
							break;
						case 5:
							for (uint32_t j = 0; j < 20; j++)
							{
								x[j] = j;
								y[j] = -1 * iq[tc][j];
							}

							load_lagrange(20, x, y);

							double max = 0;
							double eps = 10.0 * sqrt ( r8_epsilon ( ) );
							double t = eps;
							local_min(5.0, 15.0, 0.000005, 0.0001, lagrange_fun, &max);

							max_value = max;

//												test_golden();


							break;

						}


			//			printf("\n");
//						max_value = max_value + (double)rtime[0][tc] - 6.0;
//						max_value -= get_time_slot_id(max_value) * 500000;
#ifndef TD_MODE
						max_value_sum += max_value;
#else
//											if (max_value >= 9 && max_value <= 11)
//											{
							pure_maxes[filtered_count] = max_value;
							max_value = max_value * 2 + (double)rtime[0][tc] - 6.0;
//												xil_printf("Slot %u\n", get_time_slot_id(max_value));
							max_value -= get_time_slot_id(max_value) * 20000000 / SLOT_COUNT;
							max_value_sum += max_value;
//											}
//											else
//											{
//												filtered_count--;
//											}
#endif

						td_maxes[filtered_count] = max_value;
						// Вывод расстояний
//											if (calibrated_2_meters[max_counter] != 0) {
//												xil_printf("%u:\t", filtered_count);
//												print_double(1.2 + ((td_maxes[filtered_count] - calibrated_2_meters[max_counter]) * 15));
//												xil_printf(" meters\n");
//											}
						//////////////////////////////////////////////////////////////
						maxes[filtered_count] = max_value + (double)rtime[0][tc];
						if (filtered_count < TESTS_COUNT)
								filtered_count++;
					}

					if (cal_stat == 1)
					{
						max_ind_val_sum_temp += max_ind_val;
					}
				}

				if (cal_stat == 1)
				{
					max_ind_val_sum = max_ind_val_sum_temp / TESTS_COUNT;
					xil_printf("Calibrated! Mid level value is: ");
					print_double(max_ind_val_sum, 8);
					xil_printf("\n");
					cal_stat = 0;
				}

/*									max_value_sum = 0;
				filtered_count = 0;
				for(u32 i = 0; i < TESTS_COUNT; i++)
				{
					if (pure_maxes[i] >= 9 && pure_maxes[i] <= 11)
					{
						max_value_sum += td_maxes[i];
						filtered_count++;
					}
					else
					{
						for (u32 j = i; j <= TESTS_COUNT - 1; j++)
							td_maxes[j] = td_maxes[j + 1];
					}
				}
				xil_printf("Filtered count = %u\n", filtered_count);
				if (filtered_count < 50){
					xil_printf("");
				}*/
#ifdef TD_MODE
				max_value_sum /= (double)filtered_count;

				for (uint32_t ii = 0; ii < filtered_count; ii++)
				{
					max_value_dis += pow(td_maxes[ii] - max_value_sum, 2);
				}
				max_value_dis /= filtered_count;


				xil_printf("Max method %u ", max_counter);
				xil_printf("[");
				print_double(max_value_sum, 8);
				if (max_value_sum >= 10)
					xil_printf("]\t[");
				else
					xil_printf("]\t\t[");
				print_double(sqrt(max_value_dis), 8);
				xil_printf("]");
//									xil_printf("\n");
#ifdef CALIBRATION
				if (calibrated_2_meters[max_counter] == 0)
				{
					calibrated_2_meters[max_counter] = max_value_sum;
					xil_printf("Distance calibrated! 1.2 meters = ");
					print_double(max_value_sum, 6);
					xil_printf("\n");
				}

//									xil_printf("\t\tDIF= ");
//									print_double((max_value_sum - calibrated_2_meters[max_counter]) * 15);
//									xil_printf(" meters\n");

				xil_printf("\t\tDISTANCE= ");
				print_double(1.2 + ((max_value_sum - calibrated_2_meters[max_counter]) * 15), 6);
				xil_printf(" m");

				xil_printf("\t\tDISPERS= ");
				print_double(sqrt(max_value_dis) * 15, 6);
				xil_printf(" m\n");
#endif
#endif

#ifndef TD_MODE
			calculate_stat_R1(iq, rtime, maxes);
#endif
			}
//								xil_printf("\n");
//								xil_printf("*");

//			if (ret == 1)
//				break;


//							printf("Hi");
//								xil_printf("[%lf, %lf, %lf, %lf]\n", dis_delta, dis_deltamax, sum, sum_tr);
//#ifndef TD_MODE
//								calculate_stat_R1(iq, rtime, maxes);
//#endif
			tests_counter--;
//								read_corr_buffer_1(iq, rtime, maxes, blrci, blrcq, PSP_COUNT, (uint32_t) 8);
			read_specific_corr_buffer(0, iq, rtime, maxes, blrci, blrcq, PSP_COUNT, (uint32_t) 8);

//							dis_delta_array[grand_test_counter] = dis_delta;
//							dis_deltamax_array[grand_test_counter] = dis_deltamax;
//							sum_array[grand_test_counter] = sum;
//							sum_tr_array[grand_test_counter] = sum_tr;
			grand_test_counter++;

			xil_printf("-------------------\n");
//							printf("grand_test_counter = \t\t\t%u\n", grand_test_counter);
		}

//							xil_printf("-------------------\n");

	}
	return 0;
}

