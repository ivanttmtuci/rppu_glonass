/*
 * max_approx.c
 *
 *  Created on: 13 июля 2017 г.
 *      Author: Tatarchuck
 */


#include "max_approx.h"

#ifndef FIND_MAX
#define FIND_MAX

#include <math.h>
//#include <stdbool.h>

#define bool u32
#define true 1
#define false 0


//---------------------------------------------------------------------------

typedef struct pt
{
	double x, y;
} pt;
//---------------------------------------------------------------------------

typedef struct line
{
	double a, b, c;
} line;
//---------------------------------------------------------------------------

#define EPS 1e-9
//---------------------------------------------------------------------------

double det(double a, double b, double c, double d)
{
	return a * d - b * c;
}
//---------------------------------------------------------------------------

bool intersect(line m, line n, pt *res)
{
	double zn = det(m.a, m.b, n.a, n.b);
	if (abs(zn) < EPS)
		return false;
	res->x = - det(m.c, m.b, n.c, n.b) / zn;
	res->y = - det(m.a, m.c, n.a, n.c) / zn;

	return true;
}
//---------------------------------------------------------------------------

bool parallel(line m, line n)
{
	return abs(det (m.a, m.b, n.a, n.b)) < EPS;
}
//---------------------------------------------------------------------------

bool  equivalent (line m, line n) {
	return abs(det(m.a, m.b, n.a, n.b)) < EPS
	    && abs(det(m.a, m.c, n.a, n.c)) < EPS
	    && abs(det(m.b, m.c, n.b, n.c)) < EPS;
}

//---------------------------------------------------------------------------
//возвращает номер отсчета максимума, fx - само значение максимума
//s_i, s_q - дйствительная и мнимая часть входного сигнала
int find_max(double *s_i, double *s_q, double *fx)
{
  int j;
  double tmp_si, tmp_sq;
  double tmp_si1, tmp_sq1;
  double tmp_si2, tmp_sq2;
  int ret_t;

  for (j = 1; j < 19; j++)
  {
    tmp_si = s_i[j];
    tmp_sq = s_q[j];
    tmp_si1 = s_i[j-1];
    tmp_sq1 = s_q[j-1];
    tmp_si2 = s_i[j+1];
    tmp_sq2 = s_q[j+1];

    double y = sqrt(tmp_si*tmp_si + tmp_sq*tmp_sq);
    double y1 = sqrt(tmp_si1*tmp_si1 + tmp_sq1*tmp_sq1);
    double y2 = sqrt(tmp_si2*tmp_si2 + tmp_sq2*tmp_sq2);

    double ys = y2-y;
    if (ys < 0)
    {
      *fx = y;
      ret_t = j;
      break;
    }
  }

  for (j = 18; j > 0; j--)
  {
    tmp_si = s_i[j];
    tmp_sq = s_q[j];
    tmp_si1 = s_i[j+1];
    tmp_sq1 = s_q[j+1];
    tmp_si2 = s_i[j-1];
    tmp_sq2 = s_q[j-1];

    double y = sqrt(tmp_si*tmp_si + tmp_sq*tmp_sq);
    //double y1 = sqrt(tmp_si1*tmp_si1 + tmp_sq1*tmp_sq1);
    double y2 = sqrt(tmp_si2*tmp_si2 + tmp_sq2*tmp_sq2);

    double ys = y2-y;
    if (ys < 0)
    {
      *fx = (*fx + y)/2;
      ret_t = (ret_t + j)/2;
      return j;
    }
  }
  return ret_t;
}
//---------------------------------------------------------------------------
//поиск координат точек слева и справа от максимума
//
bool find_points(double *s_i, double *s_q, double cnt, double fm,
                                   double *x11, double *y11, double *x12,double *y12,
                                   double *x21, double *y21, double *x22,double *y22)
{
  int j, c;
  double tmp_si, tmp_sq;
  //int ret = -1;

  c = 0;
  for (j = 0; j < cnt; j++)
  {
    tmp_si = s_i[j];
    tmp_sq = s_q[j];

    double y = sqrt(tmp_si*tmp_si + tmp_sq*tmp_sq);
    if (y > 0.75*fm && c == 0)
    {
      *x11 = j;
      *y11 = y;
      c++;
    }
    else if (y > 0.75*fm && c > 0)
    {
      *x12 = j;
      *y12 = y;
      break;
    }
  }

  c = 0;
  for (j = 19; j > cnt; j--)
  {
    tmp_si = s_i[j];
    tmp_sq = s_q[j];

    double y = sqrt(tmp_si*tmp_si + tmp_sq*tmp_sq);
    if (y > 0.75*fm && c == 0)
    {
      *x21 = j;
      *y21 = y;
      c++;
    }
    else if (y > 0.75*fm && c > 0)
    {
      *x22 = j;
      *y22 = y;
      return true;
    }
  }

  return false;
}
//---------------------------------------------------------------------------

double est_max(double cnt, double x11, double y11, double x12,double y12,
                                     double x21, double y21, double x22,double y22)
{
  line l1, l2;
  pt maxP;

  l1.a = y12 - y11;
  l1.b = x11 - x12;
  l1.c = x11*(y11 - y12) + y11*(x12 - x11);

  l2.a = y22 - y21;
  l2.b = x21 - x22;
  l2.c = x21*(y21 - y22) + y21*(x22 - x21);

  intersect(l1, l2, &maxP);//maxP - уточненное положение максимума, расчетное

  return (double)maxP.x;
}
//-----------------------------------------------------------------------------
//возвращает уточненное значение максимума, на входе массивы квадратур,
//размерность массивов - 20
//-----------------------------------------------------------------------------
double find_est_max(double *s_i, double *s_q)
{

  double fm;
  int cnt = find_max(s_i, s_q, &fm);

  double X11, X12, X21, X22, Y11, Y12, Y21, Y22;
  bool fr = find_points(s_i, s_q, cnt, fm, &X11, &Y11, &X12, &Y12, &X21, &Y21, &X22, &Y22);
  double emax = est_max(cnt, X11, Y11, X12, Y12, X21, Y21, X22, Y22);

  return emax;
}
//---------------------------------------------------------------------------

#endif

