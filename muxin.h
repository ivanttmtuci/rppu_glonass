/*
 * muxin.h
 *
 *  Created on: 16 марта 2018 г.
 *      Author: Ivan
 */

#include "RT_AD.h"
#include "bit_field_pack.h"

#ifndef MUXIN_H
#define MUXIN_H


/*значение MUXIN по умолчанию*/
#define MUXIN_PREDEF (0x9000)

/*величины сдвигов битовых полей регистра MUXIN*/
#define MUXIN_clk_in_shr (0)
#define MUXIN_IN_shr (0x1)
#define MUXIN_pspn_shr (0x5)
#define MUXIN_DDS_shr (0x8)
#define MUXIN_DELAY_shr (0x9)
#define MUXIN_edge_to_rppu_shr (0xA)
#define MUXIN_edge_from_rppu_shr (0xB)
#define MUXIN_rppu_enable_shr (0xC)
#define MUXIN_clk_reg_shr (0xD)
#define MUXIN_SPI_SELECT_shr (0xE)
#define MUXIN_z_state_shr (0xF)
#define MUXIN_enable_sin_shr (0x10)
#define MUXIN_enable_cos_shr (0x11)
#define MUXIN_amp_dds_shr (0x12)
#define MUXIN_sel_cod_shr (0x15)
#define MUXIN_dac_out_shr (0x16)
#define MUXIN_lin_int_shr (0x17)
#define MUXIN_adc_sel_shr (0x18)
#define MUXIN_synch_scheme_shr (0x19)
#define MUXIN_iq_sel_shr (0x1A)
#define MUXIN_iq_edge_sel_shr (0x1B)
#define MUXIN_iq_dsc_sel_shr (0x1C)


/* РЕГИСТР   muxin   АДРЕС 0х43С10000  */
/* **************************************         */
struct
{
	uint32_t reg;
	struct{
		uint8_t CLK_IN:1;     /* Выбор источника опорого сигнала  Встроенный/РППУ   0/1    */
		uint8_t IN:4;     /* [4:1] разряды регистра определяют источники данных на 8-разрядных входах RE_IN и IM_IN */
		uint8_t pspn:3;     /* разряды регистра определяют уровень шума который имитирует pspn */
		uint8_t DDS:1;     /* 0   --  на выходы ПЛИС  поступает сигнал из включенного передатчика;    на выходы ПЛИС  поступает сигнал из DDS. */
		uint8_t DELAY:1;     /* разряд регистра управляет задержкой выдачи/приема IQ пар (для приема/передачи)    0 = I1Q1I2Q2       1 = Q1I2Q2I3 */
		uint8_t edge_to_rppu:1;			/* разряд регистра выбирает фронт фиксирующий входной сигнал из РППУ  BA_muxin[10] = 0   --  posedge clk ;  BA_muxin[10] = 1   --  negedge clk.*/
		uint8_t edge_from_rppu:1;     /* разряд регистра выбирает фронт фиксирующий выходной сигнал для РППУ  BA_muxin[11] = 0   --  posedge clk ;  BA_muxin[11] = 1   --  negedge clk.*/
		uint8_t rppu_enable :1;     /*Выдача сигнала Enable на РППУ  (0 – вЫкл / 1 – вкл) */
		uint8_t clk_reg :1;     /*Выбор источника тактового сигнала  0 – Clk_out_epm (прямой клок с РППУ, низкая нагрузочная способность) / 1 – Clk_out (клок проходящий через плис EPM))   0/1  */
		uint8_t SPI_SELECT :1;  /* выбор SPI контролера   Прогресс/Xilinx   0/1 */
		uint8_t z_state :1;  /* Z-state ножек SPI (0 – Normal-state / 1 – Z-state) */
		uint8_t enable_sin :1; /* 0/1 идут нули/идет синус DDS sin */
		uint8_t enable_cos :1; /* 0/1 идут нули/идет синус DDS cos */
		uint8_t amp_dds :3; /* усиление выходов ДДС */
		uint8_t sel_cod :1; /*  1 для РППУ и ЦАП /0 АД   0/1   доп код  -2048:2047  /прямой смещенной 0:4095       */
		uint8_t dac_out :1; /* 0/1  сигнал выводится на IQ_РППУ,  IQ_DAC=0  /   IQ_DAC,  IQ_РППУ=0 */
		uint8_t lin_int :1; /* линейная интерполяция НЕАКТУАЛЬНО */
		uint8_t adc_sel :1; /* ЦАП РППУ/ ЦАП ВНЕШНИЙ  0/1 */
		uint8_t synch_scheme :1; /*  0   --  Цифровой модем и РППУ работают на одном клоке (сигнал из/в РППУ идут на частоте вдвое ниже частоты клока) /= 1   --  Цифровой модем и передача сигнала из/в РППУ работают на одной частоте (номинальной) , на РППУ задается удвоенная частота от номинальной  */
		uint8_t iq_sel :1; /*выбор I/Q от внешнего ADC    BA_muxin[26] = 0   --  Входы модема : I = Iadc; Q = Qadc.   BA_muxin[26] = 1   --  Входы модема : I =Qadc; Q = Iadc.*/
		uint8_t iq_edge_sel :1; /* выбор фронта захватывающего данные с внешних ADC  BA_muxin[27] = 0   --  Входы Iadc и  Qadc захватываются восходящим фронтом clk.  BA_muxin[27] = 1   --  Входы Iadc и  Qadc захватываются нисходящим фронтом clk.*/
		uint8_t iq_dac_sel :1;  /* выбор I/Q от внешнего DAC   BA_muxin[28] = 0   --  Входы DAC подключены к выходам ПЛИС: Idac = I; Qdac = Q.   BA_muxin[28] = 1   --  Входы DAC подключены к выходам ПЛИС: Idac = Q; Qdac = I. */
		}reg_by_bit;

}muxin ;



/* Тип данных в котором храняться значения сдвигов информационных полей*/
struct{
	uint8_t clk_in;
	uint8_t IN;
	uint8_t pspn;
	uint8_t DDS;
	uint8_t DELAY;
	uint8_t edge_to_rppu;
	uint8_t edge_from_rppu;
	uint8_t rppu_enable ;
	uint8_t clk_reg ;
	uint8_t SPI_SELECT;
	uint8_t z_state;
	uint8_t enable_sin ;
	uint8_t enable_cos;
	uint8_t amp_dds;
	uint8_t sel_cod ;
	uint8_t dac_out;
	uint8_t lin_int :1;
	uint8_t adc_sel :1;
	uint8_t synch_scheme :1;
	uint8_t iq_sel :1;
	uint8_t iq_edge_sel :1;
	uint8_t iq_dac_sel :1;

}muxin_shr;



/*
Регистр кода  частоты DDS в MUX_in   -   codf[31:0];   адрес   -   BA_muxin = 0x43C10000     +  0x38;

Регистр  BA_muxin определяет источники данных на 8-разрядных входах RE_IN и IM_IN , источник тактового сигнала clk и подключение к выходу DDS.

Регистр  BA_muxin 9-разрядный [8:0] состоит из  функциональных полей разрядов : [15:9] [8] [7:5] [4:1] [0]

[0] нулевой разряд регистра определяет источник сигнала clk
             BA_muxin[0] = 0   --   clk формирует процессор ПЛИС;
             BA_muxin[0] = 1   --   clk формирует плата AD(РППУ);
[4:1] разряды регистра определяют источники данных на 8-разрядных входах RE_IN и IM_IN
             BA_muxin[4:1] = 0   --   на  входы RE_IN и IM_IN поступают сигналы с разъёма HPC Zynq;
             BA_muxin[4:1] = 1   --   на  вход RE_IN  подается выход передатчика 2 канала   RE_IN = I2[11:4] , IM_IN = 0;
             BA_muxin[4:1] = 2   --   на  входы RE_IN и IM_IN поступает ПСП имитирующая шум  RE_IN = pspn , IM_IN = pspn;
             BA_muxin[4:1] = 3   --   на  входы RE_IN и IM_IN подается выход передатчика 1 канала   RE_IN = I1 , IM_IN = Q1;
  по  lvds (CMOS в РППУ) интерфейсу   в ПЛИС поступают 12-разрядные сигналы adc_re[11:0] и adc_im[11:0].
             BA_muxin[4:1] = 4   --  RE_IN = adc_re[7:0], IM_IN = adc_im[7:0];
             BA_muxin[4:1] = 5   --  RE_IN = adc_re[8:1], IM_IN = adc_im[9:1];
             BA_muxin[4:1] = 6   --  RE_IN = adc_re[8:2], IM_IN = adc_im[9:2];
             BA_muxin[4:1] = 7   --  RE_IN = adc_re[10:3], IM_IN = adc_im[10:3];
             BA_muxin[4:1] > 7   --  RE_IN = adc_re[11:4], IM_IN = adc_im[11:4];
[7:5] разряды регистра определяют уровень шума который имитирует pspn
             BA_muxin[7:5] = 0   --  psp_n = 0;
             BA_muxin[7:5] = 1   --  psp_n = {8{psp_reg[0]}};
             BA_muxin[7:5] = 2   --  psp_n = {{6{psp_reg[1]}},psp_reg[1:0]};
             BA_muxin[7:5] = 3   --  psp_n = {{5{psp_reg[2]}},psp_reg[2:0]};
             BA_muxin[7:5] = 4   --  psp_n = {{4{psp_reg[3]}},psp_reg[3:0]};
             BA_muxin[7:5] = 5   --  psp_n = {{3{psp_reg[4]}},psp_reg[4:0]};
             BA_muxin[7:5] = 6   --  psp_n = {{2{psp_reg[5]}},psp_reg[5:0]};
             BA_muxin[7:5] = 7   --  psp_n = {{psp_reg[6]},psp_reg[6:0]};
[8]  разряд регистра подключает на выходы ПЛИС выход DDS.   (сейчас работает с РППУ)
             BA_muxin[8] = 0   --  на выходы ПЛИС  поступает сигнал из включенного передатчика;
             BA_muxin[8] = 1   --  на выходы ПЛИС  поступает сигнал из DDS.

Далее для РППУ

[9]  разряд регистра управляет задержкой выдачи/приема IQ пар (для приема/передачи)

             BA_muxin[9] = 0   -- I1Q1I2Q2
             BA_muxin[9] = 1   - -Q1I2Q2I3

[10]  разряд регистра выбирает фронт фиксирующий входной сигнал из РППУ
             BA_muxin[10] = 0   --  posedge clk ;
             BA_muxin[10] = 1   --  negedge clk.

[11]  разряд регистра выбирает фронт фиксирующий выходной сигнал для РППУ
             BA_muxin[11] = 0   --  posedge clk ;
             BA_muxin[11] = 1   --  negedge clk.

[12]  разряд регистра формирует вход  ENABLE для РППУ

[13]  разряд регистра выбирает входной клок от РППУ
             BA_muxin[13] = 0   --  clk_epm;
             BA_muxin[13] = 1   --  CLK_IN.

[14]  разряд регистра выбирает spi итерфейс
             BA_muxin[14] = 0   --  SPIM1_progress;
             BA_muxin[14] = 1   --  spi0_zynq.

[15]  разряд регистра разрешение выходов SPI
             BA_muxin[15] = 0   --  enable outs;
             BA_muxin[15] = 1   --  3-state outs.

добавленно:
[16 ] разрешение выхода sin DDS

             BA_muxin[16] = 0   --   sin_out = 0;
             BA_muxin[16] = 1   --   enable sin;

[17 ] разрешение выхода sin DDS

             BA_muxin[17] = 0   --   cos_out = 0;
             BA_muxin[17] = 1   --   enable sin;

[20:18] разряды управляют усилением выхода DDS

             BA_muxin[20:18] = 0   --  I_out = cos_DDS, Q_out = sin_DDS;
             BA_muxin[20:18] = 1   --  I_out = 2*cos_DDS, Q_out = 2*sin_DDS;
             BA_muxin[20:18] = 2   --  I_out = 4*cos_DDS, Q_out = 4*sin_DDS;
             BA_muxin[20:18] = 3   --  I_out = 8*cos_DDS, Q_out = 8*sin_DDS;
             BA_muxin[20:18] > 3   --  I_out = 16*cos_DDS, Q_out = 16*sin_DDS;

[21 ] выбор кодировки выходов sel_cod

             BA_muxin[21] = 0   --  дополнительный код сигнал от -2048 до 2047;
             BA_muxin[21] = 1   --  прямосмещенный код сигнал от 0 до 4095;

[22 ] выбор рабочих выходов IQ_РППУ или IQ_DAC

             BA_muxin[22] = 0   --   сигнал выводится на IQ_РППУ,  IQ_DAC=0;
             BA_muxin[22] = 1   --   сигнал выводится на IQ_DAC,  IQ_РППУ=0;

             Добавлено:

[23 ] включение линейной интерполяции на выходе передатчика 1-го канала , не актуально с новым фильтром.

             BA_muxin[23] = 0   --  интерполяция выключена ;
             BA_muxin[23] = 1   --  интерполяция включена;


[24 ] выбор ЦАП

             BA_muxin[24] = 0   --  Подключен ЦАП РППУ ;
             BA_muxin[24] = 1   --  Подключен внешний ЦАП;
[25] выбор схемы синхронизации передачи данных с РППУ

             BA_muxin[25] = 0   --  Цифровой модем и РППУ работают на одном клоке (сигнал из/в РППУ идут на частоте вдвое ниже частоты клока)
             BA_muxin[25] = 1   --  Цифровой модем и передача сигнала из/в РППУ работают на одной частоте (номинальной) , на РППУ задается удвоенная частота от номинальной
*/

/*extern union muxin;*/

/*установить значение MUXIN после изменения битовых полей*/
void muxin_set();

/*предустановка MUXIN*/
void muxin_set_predef();

/*включение РППУ*/
void muxin_set_rppu_on(uint8_t mode);

#endif
