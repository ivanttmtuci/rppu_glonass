/*
 * DC_Compensation.c
 *
 *  Created on: 18 мая 2018 г.
 *      Author: Korolev V.S.
 *
 */

#include "DC_compensation.h"

extern u32 enable_cnt_i_corr, enable_cnt_q_corr;

uint8_t state = COMPENSATION__IDLE_STATE;

double Zero_Q_DC_lvl = 0;
double Zero_I_DC_lvl = 0;

uint16_t cnt_i_dc_val = 0;
uint16_t cnt_q_dc_val = 0;

uint32_t cnt_i = 0;
uint32_t cnt_q = 0;
uint32_t cnt_qi = 0;

uint32_t mixer1= 0x10002;
uint32_t mixer2= 0x10001;
uint32_t msu   = 0x0;
uint32_t ifa   = 0x802E4;

u16 lvl_measure_interval = 0xFFF; // значение задержки между измерениями DC-смещения

int32_t i_dc_lvl = 0;
int32_t q_dc_lvl = 0;

uint8_t flag_end_i_corr = 0;
uint8_t flag_end_q_corr = 0;


/*
 * Функция компенсации смещения
*/
void enable_DC_correction_nodriver(uint8_t filter_type)
{
	if (enable_cnt_i_corr || enable_cnt_q_corr)
	{


		switch (filter_type)
		{
		case COMPENSATION__WIDE:
			/*измеритель включен и работает от КИХ фильтра*/
					lvl_measure_interval=0xffff;
					Xil_Out32((CH1_DC_LVL_PER), (u32)(lvl_measure_interval));
					Xil_Out32((PWR_CONT_REG), (u32)(0x1));

			break;

		case COMPENSATION__NARR:
					lvl_measure_interval=0xff;
					Xil_Out32((CH1_DC_LVL_PER), (u32)(lvl_measure_interval));
					Xil_Out32((PWR_CONT_REG), (u32)(0x3));

				break;

		}


		while (1)
		{
			switch (state)
			{
				case COMPENSATION__IDLE_STATE:
					Zero_Q_DC_lvl = 0;
					Zero_I_DC_lvl = 0;

					cnt_i_dc_val = 0x7FE0;
					cnt_q_dc_val = 0x7FE0;

					cnt_i = 0;
					cnt_q = 0;

					// Отключение МШУ
					msu_off();

					// Подготавливаем регистры для записи дефолтных значений DC-смещения (для этого метода середина диапазона значений)

					mixer1 &= (~CNT_mask);
					mixer1 |= (cnt_i_dc_val & CNT_mask);
					mixer2 &= (~CNT_mask);
					mixer2 |= (cnt_q_dc_val & CNT_mask);

					// Записываем дефолтные знаячения DC-коррекции в РППУ
					write_CNT_regs();

					state = COMPENSATION__STEP_1;
				break;

				case COMPENSATION__STEP_1:
					read_DC_lvl();

					if ( fabs(Zero_I_DC_lvl) > fabs(2.00) )
					{
						cnt_i++;
						if (cnt_i < 10)
						{
							if (Zero_I_DC_lvl > 0)
							{
								cnt_i_dc_val -= ((0x7FE0 >> cnt_i) & CNT_mask);

								mixer1 &= ~CNT_mask;
								mixer1 |= cnt_i_dc_val;
							}
							else
							{
								cnt_i_dc_val += ((0x7FE0 >> cnt_i) & CNT_mask);
								mixer1 &= ~CNT_mask;
								mixer1 |= cnt_i_dc_val;
							}
							rppu_write_reg(mixer1);
						}
						else
						{
							state = COMPENSATION__STEP_2;
						}
					}
					else
					{
						state = COMPENSATION__STEP_2;
					}

				break;

				case COMPENSATION__STEP_2:
					read_DC_lvl();

					if ( fabs(Zero_Q_DC_lvl) > fabs(2.00) )
					{
						cnt_q++;
						if (cnt_q < 10)
						{
							if (Zero_Q_DC_lvl > 0)
							{
								cnt_q_dc_val += ( (0x7FE0 >> cnt_q) & CNT_mask );
								mixer2 &= ~CNT_mask;
								mixer2 |= cnt_q_dc_val;
							}
							else
							{
								cnt_q_dc_val -= ( (0x7FE0 >> cnt_q) & CNT_mask );
								mixer2 &= ~CNT_mask;
								mixer2 |= cnt_q_dc_val;
							}

							rppu_write_reg(mixer2);
						}
						else
						{
							state = COMPENSATION__STEP_3;
						}
					}
					else
					{
						state = COMPENSATION__STEP_3;
					}
				break;

				case COMPENSATION__STEP_3:
					read_DC_lvl();

					if ( ( fabs(Zero_I_DC_lvl) > fabs(1.00) )
					  || ( fabs(Zero_Q_DC_lvl) > fabs(1.00) ) )
					{
						cnt_qi++;
						if (cnt_qi < 150)
						{
							if (Zero_I_DC_lvl > 0)
							{
								cnt_i_dc_val -= 0x20 ;
								mixer1 &= ~CNT_mask;
								mixer1 |= cnt_i_dc_val;
							}
							else
							{
								cnt_i_dc_val += 0x20;
								mixer1 &= ~CNT_mask;
								mixer1 |= cnt_i_dc_val;
							}
							rppu_write_reg(mixer1);

							if (Zero_Q_DC_lvl > 0)
							{
								cnt_q_dc_val += 0x20 ;
								mixer2 &= ~CNT_mask;
								mixer2 |= cnt_q_dc_val;
							}
							else
							{
								cnt_q_dc_val -= 0x20;
								mixer2 &= ~CNT_mask;
								mixer2 |= cnt_q_dc_val;
							}
							rppu_write_reg(mixer2);
						}
						else
						{
							state = COMPENSATION__END;
						}
					}
					else
					{
						state = COMPENSATION__END;
					}
				break;

				case COMPENSATION__END:
				{
					if (cnt_qi == 150)
					{
						xil_printf("Correction failed!\n");
					}
					else
					{
						xil_printf("Successfully corrected I and Q levels!\n");
					}

					xil_printf("cnt_qi.... %d \n ", cnt_qi);

					xil_printf("cnt_i_dc_val .... %08x \n ", cnt_i_dc_val);
					xil_printf("I_DC_LVL = ");
					print_double(Zero_I_DC_lvl, 8);
					xil_printf("\n");

					xil_printf("cnt_q_dc_val .... %08x \n ", cnt_q_dc_val);
					xil_printf("Q_DC_LVL = ");
					print_double(Zero_Q_DC_lvl, 8);
					xil_printf("\n");

					state = COMPENSATION__IDLE_STATE;

					enable_cnt_i_corr = 0;
					enable_cnt_q_corr = 0;
					//Включаем МШУ
					msu_on();

					return;
				}
				break;
			}
		}
	}
}

void read_DC_lvl (void)
{
	/*
	 * Сброс регистров осуществляется прочтением регистра Q-канала,
	 * поэтому перед получением текущего значения вычитывает регистр Q-канала и
	 * выжидаем максимальное время задержки.
	 */


	q_dc_lvl = Xil_In32(CH1_DC_Q_LVL);
	mdelay(50);

	while((Xil_In32(CH1_PWR_FLG)&0x2)==0)
	{
		xil_printf("REG = %u \n",Xil_In32(CH1_PWR_FLG));
	}


	i_dc_lvl = Xil_In32(CH1_DC_I_LVL);
	q_dc_lvl = Xil_In32(CH1_DC_Q_LVL);

	Zero_I_DC_lvl = i_dc_lvl / (double)lvl_measure_interval;
	Zero_Q_DC_lvl = q_dc_lvl / (double)lvl_measure_interval;

}

void read_and_print_DC_lvl (void)
{
	/*
	 * Сброс регистров осуществляется прочтением регистра Q-канала,
	 * поэтому перед получением текущего значения вычитывает регистр Q-канала и
	 * выжидаем максимальное время задержки.
	 */
	q_dc_lvl = Xil_In32(CH1_DC_Q_LVL);
	mdelay(50);

	while((Xil_In32(CH1_PWR_FLG)&0x2)==1)
			continue;

	i_dc_lvl = Xil_In32(CH1_DC_I_LVL);
	q_dc_lvl = Xil_In32(CH1_DC_Q_LVL);

	Zero_I_DC_lvl = i_dc_lvl / (double)lvl_measure_interval;
	Zero_Q_DC_lvl = q_dc_lvl / (double)lvl_measure_interval;

	xil_printf("I_DC_LVL = ");
	print_double(Zero_I_DC_lvl, 8);
	xil_printf("\n");

	xil_printf("Q_DC_LVL = ");
	print_double(Zero_Q_DC_lvl, 8);
	xil_printf("\n");
}

void write_CNT_regs(void)
{
	rppu_write_reg(mixer2);
	rppu_write_reg(mixer1);
}

void msu_off(void)
{
	msu &= ~IN_S;
	rppu_write_reg(msu);
}

void msu_on(void)
{
	msu |= IN_S;
	rppu_write_reg(msu);
}

void ifa_write (uint8_t ifa_val)
{
	ifa &= (~IFA_mask);
	ifa |= (((u32)ifa_val) << IFA_shift) & IFA_mask;
	rppu_write_reg(ifa);
}

void IFA_Change(void)
{
	for(uint8_t iii = 0; iii <= 51; iii++)
	{

		ifa_write(iii);
		xil_printf("ifa.... %d \n ", iii);

		enable_cnt_i_corr = 1;
		enable_cnt_q_corr = 1;

		enable_DC_correction_nodriver(COMPENSATION__WIDE);
	}
	enable_cnt_i_corr = 0;
	enable_cnt_q_corr = 0;
}

