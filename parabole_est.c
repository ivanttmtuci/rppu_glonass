double det_third( double a11, double a12,double a13,
				  double a21, double a22,double a23,
				  double a31, double a32,double a33 )
{
	double det=0;
	
	

	det= a11*a22*a33
		+a12*a23*a31
		+a13*a21*a32
		-a13*a22*a31
		-a11*a23*a32
		-a12*a21*a33;


	return det;
}

double approx_parabola(double *iq,double *x, int x_start, int x_end, double *max_x, double *max_y,double * aa, double *bb, double *cc)
{
		double max_est=0;
		
	
		
		double sy,sxy,sx2y,sx,sx2,sx3,sx4=0;

	
		double a=0;
		double b=0;
		double c=0;

		double xv=0;
		double yv=0;


		int i=0;
		int n=20;

		double D=0;
		double D1=0;
		double D2=0;
		double D3=0;


		sx=0;
		sxy=0;
		sy=0;
		sx2y=0;
		sx2=0;
		sx3=0;
		sx4=0;

	



		for(i=x_start;i<=x_end;i++)
		{
			/*sx+=*(iq+i);
			sx2+=pow(*(iq+i),2);
			sx3+=pow(*(iq+i),3);
			sx4+=pow(*(iq+i),4);*/
			
			sx+=*(x+i);
			sx2+=pow(*(x+i),2);
			sx3+=pow(*(x+i),3);
			sx4+=pow(*(x+i),4);

			
			sy+=(*(iq+i));
			sxy+=(*(iq+i))*(*(x+i));
			sx2y+=( pow(*(x+i) ,2)   )   *(*(iq+i));

		}

		


		D=det_third(n,sx,sx2,sx,sx2,sx3,sx2,sx3,sx4);

		D1=det_third(sy,sxy,sx2y,sx,sx2,sx3,sx2,sx3,sx4);

		D2=det_third(n,sx,sx2,sy,sxy,sx2y,sx2,sx3,sx4);

		D3=det_third(n,sx,sx2,sx,sx2,sx3,sy,sxy,sx2y);

		a=D1/D;
		b=D2/D;
		c=D3/D;

		*aa=a;
		*bb=b;
		*cc=c;

		xv = -b / (2*c);
		yv = c - b*b / (4*a);
		
		*max_x=xv;
		*max_y=yv;
		return xv;
}


double intersect2(double a1,double b1,double c1,double a2,double b2,double c2,double *xp,double *yp)
{

	double x1,x2;
	double m,p,k;

	m=c1-c2;
	p=b1-b2;
	k=a1-a2;

	x1=(-p+(sqrt(p*p-4*m*k)))/(2*m);
	x2=(-p-(sqrt(p*p-4*m*k)))/(2*m);

	*xp=x1;
	*yp=x1*x1*c1+x1*b1+a1;
	

	return x2;


}

double parabole_estimation(double *iq,int max)
{

	

	double x[20];
	int i;
	double D;


	double a1;
	double b1;
	double c1;
	
	double a2;
	double b2;
	double c2;

	double xmax=0;
	double ymax=0;


	double *ap1;
	double *bp1;
	double *cp1;

	double *ap2;
	double *bp2;
	double *cp2;

	
	double xper,yper;
	double *xperp,*yperp;



	for(i=0;i<20;i++)
		{
			x[i]=(float)i;		

		}

		
	
	ap1=&a1;
	bp1=&b1;
	cp1=&c1;

	ap2=&a2;
	bp2=&b2;
	cp2=&c2;

	xperp=&xper;
	yperp=&yper;


	D=approx_parabola(iq,x,max-2,max,&xmax,&ymax,ap1,bp1,cp1);
	D=approx_parabola(iq,x,max,max+2,&xmax,&ymax,ap2,bp2,cp2);

	D=intersect2(a1, b1, c1, a2, b2,c2,xperp,yperp);
	


	return xper;

}

double RMS(double *data, int len)
{

	double rms_rez=0;

	double mo=0;

	int i;

	for(i=0;i<len;i++)
	{
		mo+=*(data+i);

	}
	mo=mo/len;


	for(i=0;i<len;i++)
	{
		rms_rez=pow((*(data+i)-mo),2);

	}
	rms_rez=sqrt(rms_rez/len);

	return rms_rez;

}

double approx_parabola2(double *iq,int x_start, int x_end)
{
		double max_est=0;
		
	
		
		double sy,sxy,sx2y,sx,sx2,sx3,sx4=0;

	
		double a=0;
		double b=0;
		double c=0;

		double xv=0;
		double yv=0;


		int i=0;
		int n=20;

		double D=0;
		double D1=0;
		double D2=0;
		double D3=0;

		float x[20];


		sx=0;
		sxy=0;
		sy=0;
		sx2y=0;
		sx2=0;
		sx3=0;
		sx4=0;


		for(i=0;i<20;i++)
		{
			x[i]=(float)i;
		

		}



		for(i=x_start;i<=x_end;i++)
		{
			
			sx+=x[i];
			sx2+=pow(x[i],2);
			sx3+=pow(x[i],3);
			sx4+=pow(x[i],4);

			
			sy+=(*(iq+i));
			sxy+=(*(iq+i))*(x[i]);
			sx2y+=( pow(x[i] ,2)   )   *(*(iq+i));

		}

		


		D=det_third(n,sx,sx2,sx,sx2,sx3,sx2,sx3,sx4);

		D1=det_third(sy,sxy,sx2y,sx,sx2,sx3,sx2,sx3,sx4);

		D2=det_third(n,sx,sx2,sy,sxy,sx2y,sx2,sx3,sx4);

		D3=det_third(n,sx,sx2,sx,sx2,sx3,sy,sxy,sx2y);

		a=D1/D;
		b=D2/D;
		c=D3/D;

	

		xv = -b / (2*c);
		yv = c - b*b / (4*a);
		
	
		return xv;
}
