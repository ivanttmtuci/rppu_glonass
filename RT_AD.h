/*
 * RT1.h
 *
 *  Created on: 3 мая 2017 г.
 *      Author: Tatarchuck
 */

/*
#define BASE_ADR 0x43C00000

#define BA_RTC   0x43C1C080
#define BA_RC    0x43C1C100
#define BA_R2    0x43C00800 //В коде Саши от 20.03.2018 был #define BA_R2    0x43C04000
#define BA_R1    0x43C00000
#define BA_T1    0x43C00C00 ////В коде Саши от 20.03.2018 был #define BA_R2    0x43C08000
#define BA_T2    0x43C00C40
#define BA_GPSP  0x43C00D00
#define BA_mem   0x43C20000
#define BA_muxin 0x43C10000
#define BA_ETALON 	0x43C00E00

#define BA_SPI1  0x43C1C2C0
*/

/*
 * Дефайны от 20.03.2018
 */
/*
#define BASE_ADR 0x43C00000
#define BA_RTC   0x43C1C080
#define BA_RC    0x43C1C100
#define T2_BASE  0x43C09000
#define R2_BASE  0x43C04000
#define BA_muxin 0x43C10000
#define BA_SPI1  0x43C1C2C0

#define BA_R2    0x43C04000
#define BA_R1    0x43C00000
#define BA_T1    0x43C08000
#define BA_T2    0x43C09000
#define BA_GPSP  0x43C0C000
#define BA_mem   0x43C20000
*/

/*
 * Дефайны от 20,04,18 внешний АЦП + ЦАП
 */
#define BASE_ADR 0x43C00000
#define BA_RTC   0x43C1C080
#define BA_RC    0x43C1C100
#define T2_BASE  0x43C09000
#define R2_BASE  0x43C04000
#define BA_muxin 0x43C10000
#define BA_SPI1  0x43C1C2C0


#define BA_R2    0x43C04000
#define BA_R1    0x43C00000
#define BA_R1k1  0x43C0A000 //common FIR
#define BA_T1    0x43C08000
#define BA_T1k1  0x43C08100

#define BA_PWR   0x43C0A100

#define BA_T2    0x43C09000
#define BA_GPSP  0x43C0C000
#define BA_ET    0x43C0C100
#define BA_mem   0x43C20000








#define PWR_CONT_REG		(BA_PWR  + 0x0000) // x16


#define CH1_PWR_PER			(BA_PWR  + 0x0004) // x32
#define CH1_DC_LVL_PER		(BA_PWR  + 0x0008) // x32Безнаковое число
#define CH1_PWR_BUFF   		(BA_PWR  + 0x000C) // 32 Беззнаковое число

#define CH1_M_I   		    (BA_PWR  + 0x0010) // 32 Беззнаковое число
#define CH1_M_Q   		    (BA_PWR  + 0x0014) // 32 Беззнаковое число

#define CH1_DC_I_LVL   		(BA_PWR  + 0x0018) // 32 Знаковое
#define CH1_DC_Q_LVL  		(BA_PWR  + 0x001C) // 32 Знаковое

#define CH1_PWR_FLG  		(BA_PWR  + 0x0020) // 2 bit


#include <stdio.h>
#include <stdint.h>
#include "xparameters.h"
#include "xil_cache.h"
#include "xscugic.h"
#include "xil_exception.h"
#include "xdmaps.h"
#include "xdevcfg.h"
#include "xscutimer.h"
#include "xscuwdt.h"
#include "math.h"
#include "stdlib.h"
#include "string.h"
#include <assert.h>


#include "debug_func.h"
#include <xil_cache.h>




// defines for Valeriy's code
/*
#define MODEM_BASE 		0x43C00000
#define GPSP_MEM_BASE	0x43C20000
#define GPSP_R_BASE  	0x43C00D00
#define R1_BASE        	0x43C00000
#define T1_BASE       	0x43C00C00
#define R2_BASE        	0x43C00800
#define T2_BASE        	0x43C00C40
#define MP_RC_BASE    	0x43C1C100
*/


/*
#define  MODEM_BASE     (0x50000000)
#define  GPSP_MEM_BASE  (0x50020000)
#define  GPSP_R_BASE    (0x50000D00)
#define  R1_BASE        (0x50000000)
#define  T1_BASE        (0x50000C00)
#define  R2_BASE        (0x50000800)
#define  T2_BASE        (0x50000C40)
#define  POW_BASE       (0x50000C80)
#define  SPI_BASE       (0x4000C2C0)
#define  GPIO_BASE     	(0x4000C200)
*/

