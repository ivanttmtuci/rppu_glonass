/*
 * lagrange.h
 *
 *  Created on: 5 июня 2018 г.
 *      Author: ProRock
 */

#ifndef SRC_LAGRANGE_H_
#define SRC_LAGRANGE_H_
#include "RT1_functions.h"
#include <stdio.h>
#include <math.h>

double Li(int i, int n, double x[n], double X);
double Pn(int n, double x[n], double y[n], double X);
u32 test_golden();
void bisection (double *x, double a, double b, int *itr);
double lagrange_fun(double x);
void load_lagrange(int n, double x[n], double y[n]);


#define MAX_N 21
int N;
double X_ARR[MAX_N], Y_ARR[MAX_N];

#endif /* SRC_LAGRANGE_H_ */
