/*
 * lagrange.c
 *
 *  Created on: 5 июня 2018 г.
 *      Author: ProRock
 */

#include "lagrange.h"


/*Function to evaluate Li(x)*/

double Li(int i, int n, double x[n], double X){
	int j;
	double prod=1;
    for(j=0;j<n;j++){
        if(j!=i)
        	prod=prod*(X-x[j])/(x[i]-x[j]);
	}
        return prod;
}

/*Function to evaluate Pn(x) where Pn is the Lagrange interpolating polynomial of degree n*/
// Тут было n + 1
double Pn(int n, double x[n], double y[n], double X){
	double sum=0;
	int i;
    for(i=0;i<n;i++){
        sum=sum+Li(i,n,x,X)*y[i];
    }
    return sum;
}

void load_lagrange(int n, double x[n+1], double y[n+1])
{
	N = n;
	for (u32 i = 0; i < n; i++) {
		X_ARR[i] = x[i];
		Y_ARR[i] = y[i];
	}
}

double lagrange_fun(double x) {
	// final result
	return Pn(N, X_ARR, Y_ARR, x);
}


u32 test_golden()
{

	double yy[20] = {0};
	for (u32 i = 0; i < 20; i++) {
		yy[i] = lagrange_fun(i);
	}

	return 0;
}
