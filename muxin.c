/*
 * muxin.c
 *
 *  Created on: 16 марта 2018 г.
 *      Author: Ivan
 */


#include "muxin.h"
#include "muxin.h"
#include "bit_field_pack.h"


/* РЕГИСТР   muxin   АДРЕС 0х43С10000  */
/* **************************************         */



/*установить значение MUXIN после изменения битовых полей*/
void muxin_set()
{

	uint32_t reg=0;

	reg=bit_field_pack_u32(reg,muxin.reg_by_bit.CLK_IN,(u8) muxin_shr.clk_in,(u8) 0x1);
	reg=bit_field_pack_u32(reg,muxin.reg_by_bit.IN,(u8) muxin_shr.IN,(u8) 0xF);
	reg=bit_field_pack_u32(reg,muxin.reg_by_bit.pspn,(u8) muxin_shr.pspn,(u8) 0x7);
	reg=bit_field_pack_u32(reg,muxin.reg_by_bit.DDS,(u8) muxin_shr.DDS,(u8) 0x1);
	reg=bit_field_pack_u32(reg,muxin.reg_by_bit.DELAY,(u8) muxin_shr.DELAY,(u8) 0x1);
	reg=bit_field_pack_u32(reg,muxin.reg_by_bit.edge_to_rppu,(u8) muxin_shr.edge_to_rppu,(u8) 0x1);
	reg=bit_field_pack_u32(reg,muxin.reg_by_bit.edge_from_rppu,(u8) muxin_shr.edge_from_rppu,(u8) 0x1);
	reg=bit_field_pack_u32(reg,muxin.reg_by_bit.rppu_enable,(u8) muxin_shr.rppu_enable,(u8) 0x1);
	reg=bit_field_pack_u32(reg,muxin.reg_by_bit.clk_reg,(u8) muxin_shr.clk_reg,(u8) 0x1);
	reg=bit_field_pack_u32(reg,muxin.reg_by_bit.SPI_SELECT,(u8) muxin_shr.SPI_SELECT,(u8) 0x1);
	reg=bit_field_pack_u32(reg,muxin.reg_by_bit.z_state,(u8) muxin_shr.z_state,(u8) 0x1);
	reg=bit_field_pack_u32(reg,muxin.reg_by_bit.enable_sin,(u8) muxin_shr.enable_sin,(u8) 0x1);
	reg=bit_field_pack_u32(reg,muxin.reg_by_bit.enable_cos,(u8) muxin_shr.enable_cos,(u8) 0x1);
	reg=bit_field_pack_u32(reg,muxin.reg_by_bit.amp_dds,(u8) muxin_shr.amp_dds,(u8) 0x7);
	reg=bit_field_pack_u32(reg,muxin.reg_by_bit.sel_cod,(u8) muxin_shr.sel_cod,(u8) 0x1);
	reg=bit_field_pack_u32(reg,muxin.reg_by_bit.dac_out,(u8) muxin_shr.dac_out,(u8) 0x1);
	reg=bit_field_pack_u32(reg,muxin.reg_by_bit.lin_int,(u8) muxin_shr.lin_int,(u8) 0x1);
	reg=bit_field_pack_u32(reg,muxin.reg_by_bit.adc_sel,(u8) muxin_shr.adc_sel,(u8) 0x1);
	reg=bit_field_pack_u32(reg,muxin.reg_by_bit.synch_scheme,(u8) muxin_shr.synch_scheme,(u8) 0x1);
	muxin.reg=reg;
	Xil_Out32((BA_muxin),        (u32)(reg));

}

/*Установка значения MUXIN по умолчанию MUXIN_PREDEF */
void muxin_set_predef()
{
	muxin.reg=MUXIN_PREDEF;

	muxin_shr.clk_in=MUXIN_clk_in_shr;
	muxin_shr.IN=MUXIN_IN_shr;
	muxin_shr.pspn=MUXIN_pspn_shr;
	muxin_shr.DDS=MUXIN_DDS_shr;
	muxin_shr.DELAY=MUXIN_DELAY_shr;
	muxin_shr.edge_to_rppu= MUXIN_edge_to_rppu_shr;
	muxin_shr.edge_from_rppu=MUXIN_edge_from_rppu_shr;
	muxin_shr.rppu_enable=MUXIN_rppu_enable_shr;
	muxin_shr.clk_reg=MUXIN_clk_reg_shr;
	muxin_shr.SPI_SELECT=MUXIN_SPI_SELECT_shr;
	muxin_shr.z_state=MUXIN_z_state_shr;
	muxin_shr.enable_sin=MUXIN_enable_sin_shr ;
	muxin_shr.enable_cos=MUXIN_enable_cos_shr;
	muxin_shr.amp_dds=MUXIN_amp_dds_shr;
	muxin_shr.sel_cod=MUXIN_sel_cod_shr;
	muxin_shr.dac_out=MUXIN_dac_out_shr;
	muxin_shr.iq_sel=MUXIN_iq_sel_shr;
	muxin_shr.iq_edge_sel=MUXIN_iq_edge_sel_shr;
	muxin_shr.iq_dac_sel=MUXIN_iq_dsc_sel_shr;

	Xil_Out32((BA_muxin),        (u32)(muxin.reg));
}


/*включение РППУ
 *
 *mode >0 -  пеередача
 *mode =0 прием
 * */
void muxin_set_rppu_on(uint8_t mode)
{

	muxin_set_predef();

	/*Настройка выходного мультиплексора для работы с РППУ*/
	muxin.reg_by_bit.rppu_enable=1;
	muxin.reg_by_bit.SPI_SELECT=0;
	muxin.reg_by_bit.z_state=0;
	muxin.reg_by_bit.CLK_IN=1;
	muxin.reg_by_bit.clk_reg=1;
	muxin.reg_by_bit.edge_from_rppu=1;

	if(mode>0)
	{
	muxin.reg_by_bit.sel_cod=1; /*1 внеш цап*/
	muxin.reg_by_bit.dac_out=1;
	}
	else
	{
	muxin.reg_by_bit.sel_cod=0;
	muxin.reg_by_bit.adc_sel=1;
	muxin.reg_by_bit.synch_scheme=0;

	}


	muxin.reg_by_bit.IN=0;  /*4 внуьр АЦП*/
	muxin.reg_by_bit.DDS=0;

	muxin_set();

}
